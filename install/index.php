<?php
defined('B_PROLOG_INCLUDED') || die;
use Biotum\Manufacture\Entity\BidsTable;
use Bitrix\Main\Entity;
use Bitrix\Main\Application;
use Bitrix\Main\Config\Option;
use Bitrix\Main\EventManager;
use Bitrix\Main\Loader;
use Bitrix\Main\Localization\Loc;
use Bitrix\Main\ModuleManager;

class biotum_manufacture extends CModule
{
    const MODULE_ID = 'biotum.manufacture';
    var $MODULE_ID = self::MODULE_ID;
    var $MODULE_VERSION;
    var $MODULE_VERSION_DATE;
    var $MODULE_NAME;
    var $MODULE_DESCRIPTION;
    var $MODULE_CSS;
    var $strError = '';

    function __construct()
    {
        $arModuleVersion = array();
        include(dirname(__FILE__) . '/version.php');
        $this->MODULE_VERSION = $arModuleVersion['VERSION'];
        $this->MODULE_VERSION_DATE = $arModuleVersion['VERSION_DATE'];

        $this->MODULE_NAME = Loc::getMessage('BIOTUM_CRMBIDS.MODULE_NAME');
        $this->MODULE_DESCRIPTION = Loc::getMessage('BIOTUM_CRMBIDS.MODULE_DESC');

        $this->PARTNER_NAME = Loc::getMessage('BIOTUM_CRMBIDS.PARTNER_NAME');
        $this->PARTNER_URI = Loc::getMessage('BIOTUM_CRMBIDS.PARTNER_URI');
    }

    function DoInstall()
    {
        ModuleManager::registerModule(self::MODULE_ID);

        $this->InstallDB();
        $this->InstallFiles();
        $this->InstallEvents();
        $this->InstallPropertyProduct();
    }

    function DoUninstall()
    {
        $this->UnInstallEvents();
        $this->UnInstallFiles();
        $this->UnInstallDB();
        ModuleManager::unRegisterModule(self::MODULE_ID);
    }

    function InstallDB()
    {
        Loader::includeModule('biotum.manufacture');

        $db = Application::getConnection();

       $storeEntity = BidsTable::getEntity();
        if (!$db->isTableExists($storeEntity->getDBTableName())) {
            $storeEntity->createDbTable();
        }


    }

    function InstallPropertyProduct(){

        $arFields = Array(
            "NAME" => "Время производства (часы)",
            "ACTIVE" => "Y",
            "SORT" => "100",
            "CODE" => "TIME_MANUFACTURE",
            "PROPERTY_TYPE" => "N",
            "IBLOCK_ID" =>27
        );

        $iblockproperty = new \CIBlockProperty;
        $iblockproperty->Add($arFields);

    }

    function UnInstallDB()
    {

        // Не существенно в данном примере.
    }

    function InstallEvents()
    {

        $eventManager = EventManager::getInstance();

        $eventManager->registerEventHandlerCompatible(
            'crm',
            'OnAfterCrmControlPanelBuild',
            self::MODULE_ID,
            '\Biotum\Manufacture\Handler\CrmMenu',
            'addBids'
        );

        $eventManager->registerEventHandlerCompatible(
            'rest',
            'OnRestServiceBuildDescription',
            self::MODULE_ID,
            '\Biotum\Manufacture\Handler\Rest',
            'registerInterface'
        );



    }

    function UnInstallEvents()
    {
        $eventManager = EventManager::getInstance();

        $eventManager->unRegisterEventHandler(
            'crm',
            'OnAfterCrmControlPanelBuild',
            self::MODULE_ID,
            '\Biotum\Manufacture\Handler\CrmMenu',
            'addBids'
        );

    }

    function InstallFiles()
    {
        $documentRoot = Application::getDocumentRoot();

        CopyDirFiles(
            __DIR__ . '/files/admin',
            $documentRoot . '/bitrix/admin',
            true,
            true
        );
        CopyDirFiles(
            __DIR__ . '/files/custom',
            $documentRoot . '/bitrix/activities',
            true,
            true
        );

        CopyDirFiles(
            __DIR__ . '/files/components',
            $documentRoot . '/local/components',
            true,
            true
        );

        CopyDirFiles(
            __DIR__ . '/files/pub/crm',
            $documentRoot . '/crm',
            true,
            true
        );

        CUrlRewriter::Add(array(
            'CONDITION' => '#^/crm/bids/#',
            'RULE' => '',
            'ID' => 'biotum.manufacture:bid',
            'PATH' => '/crm/bids/index.php',
        ));
    }

    function UnInstallFiles()
    {
        DeleteDirFilesEx('/crm/bids');
        DeleteDirFilesEx('/local/components/biotum.manufacture');

        CUrlRewriter::Delete(array(
            'ID' => 'biotum.manufacture:bid',
            'PATH' => '/crm/bids/index.php',
        ));
    }
}