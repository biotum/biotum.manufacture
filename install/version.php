<?php
defined('B_PROLOG_INCLUDED') || die;

$arModuleVersion = array(
    'VERSION' => '1.0.0',
    'VERSION_DATE' => '2019-11-08 09:45:14'
);