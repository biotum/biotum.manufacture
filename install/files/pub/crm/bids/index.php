<?php
require($_SERVER['DOCUMENT_ROOT'].'/bitrix/header.php');

$APPLICATION->IncludeComponent(
	'biotum.manufacture:bid',
	'', 
	array(
		'SEF_MODE' => 'Y',
		'SEF_FOLDER' => '/crm/bids/',
		'SEF_URL_TEMPLATES' => array(
			'details' => '#BID_ID#/',
			'edit' => '#BID_ID#/edit/',
		)
	),
	false
);

require($_SERVER['DOCUMENT_ROOT'].'/bitrix/footer.php');