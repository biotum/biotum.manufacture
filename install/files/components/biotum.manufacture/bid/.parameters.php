<?php
defined('B_PROLOG_INCLUDED') || die;

use Bitrix\Main\Localization\Loc;

$arComponentParameters = array(
    'PARAMETERS' => array(
        'SEF_MODE' => array(
            'details' => array(
                'NAME' => Loc::getMessage('CRMBIDS_DETAILS_URL_TEMPLATE'),
                'DEFAULT' => '#BID_ID#/',
                'VARIABLES' => array('BID_ID')
            ),
            'edit' => array(
                'NAME' => Loc::getMessage('CRMBIDS_EDIT_URL_TEMPLATE'),
                'DEFAULT' => '#BID_ID#/edit/',
                'VARIABLES' => array('BID_ID')
            ),'terminal' => array(
                'NAME' => Loc::getMessage('CRMBIDS_EDIT_URL_TEMPLATE'),
                'DEFAULT' => 'terminal/',
                'VARIABLES' => array('BID_ID')
            ),

        )
    )
);
