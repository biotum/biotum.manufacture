<?php
defined('B_PROLOG_INCLUDED') || die;


use Biotum\Manufacture\Entity\ManufacturreBids;
use Bitrix\Main\Application;

///** @var CBitrixComponentTemplate $this */
CJSCore::Init(array("jquery"));
$APPLICATION->SetTitle('MONITOR');
////define('VUEJS_DEBUG', true);

$this->addExternalCss($this->GetFolder() ."/grid/main.css");
$this->addExternalCss($this->GetFolder() ."/grid/jsgrid.css");
$this->addExternalJS($this->GetFolder() ."/grid/jsgrid.js");
$this->addExternalJS($this->GetFolder() ."/grid/coolclock.js");

$status = json_decode(COption::GetOptionString('biotum.manufacture', 'BIDS_FIELD_DESTINATION'), true);

$fields=[];
foreach ($status as $field=>$entity){

    $fields[]=['name'=>$field,'type'=>'text'];


}

print_r(ManufacturreBids::getlist());

?>

<div id="bx_recall_popup_form" style="" class="bx_popup_form" >

<p>
    <button onclick="logger.clear()">Clear log</button>
    <button onclick="closeConnection()">Stop reconnections</button>
    <span id="connection">Connecting...<div></div></span>
</p>
<div class="border" id="log"></div>
<!--    <canvas class="CoolClock::140:::showDigital" title="Server Sent Events are powering this clock"></canvas>-->
<div class="table-wrap" id="right-panel">

       <div id="jsGrid"></div>
</div>

</div>

<a href="javascript:void(0)" onclick="openRecallPopup()" class="recall">показать на весь экран</a>
<script>
    $('#jsGrid').jsGrid({
        width: "auto",
        height: "auto",
         autoload:   true,
        inserting: false,
        editing: false,
        sorting: false,
        paging: false,
        filtering: false,
         controller : {
            loadData: function() {
                var d = $.Deferred();
                $.ajax({
                    type: "POST",
                    url: "/local/components/biotum.manufacture/bid/ajax.php",
                    dataType: "json"
                }).done(function(response) {
                    d.resolve(response);
                    console.log(response);
                });

                return d.promise();
            }
        },
        headerRowRenderer: function() {
            var $result = $("<tr class='jsgrid-header-row'>").height(0)
                .append($("<th class='jsgrid-header-row' style='text-align:center; background-color:#8e7cc3;'>").attr("colspan",11).text("#1 КОММЕРЧЕСКИЙ ОТДЕЛ"))
                .append($("<th class='jsgrid-header-row' style='text-align:center; background-color:#e06666;'>").attr("colspan",3).text("#2 ПРОИЗВОДСТВО"))
                .append($("<th class='jsgrid-header-row' style='text-align:center; background-color:#999999;'>").attr("colspan",5).text("#3 ЛОГИСТИКА"))
                .append($("<th class='jsgrid-header-row' style='text-align:center; background-color:#f9cb9c;'>").attr("colspan",6).text("#4 СЕРВИСНАЯ СЛУЖБА"));

            $result = $result.add($("<tr class='jsgrid-header-row'>")
                .append($("<th class='jsgrid-header-row' style='background-color:#8e7cc3;'>").attr("colspan",11).text(""))
                .append($("<th class='jsgrid-header-row' style='text-align:center; background-color:#e06666;  font:14px Arial, sans-serif;'>").attr("colspan",3).text("ОПЕРАТОР ПБВ"))
                .append($("<th class='jsgrid-header-row' style='text-align:center; background-color:#999999;  font:14px Arial, sans-serif;'>").attr("colspan",5).text("ОПЕРАТОР ТС"))
                .append($("<th class='jsgrid-header-row' style='text-align:center; background-color:#f9cb9c;  font:14px Arial, sans-serif;'>").attr("colspan",6).text("ОПЕРАТОР ВП")))


            $result = $result.add($("<tr class='jsgrid-header-row'>")
                .append($("<th class='jsgrid-header-row' style='background-color:#8e7cc3;'>").attr("colspan",11).text("Регистрация заявки"))
                .append($("<th class='jsgrid-header-row' style='text-align:center; background-color:#e06666; font:14px Arial, sans-serif;'>").attr("colspan",2).text("Готовность к наливу"))
                .append($("<th class='jsgrid-header-row' style='text-align:center; background-color:#e06666; font:14px Arial, sans-serif;'>").text("Коррект."))
                .append($("<th class='jsgrid-header-row' style='text-align:center; background-color:#999999; font:14px Arial, sans-serif;'>").attr("colspan",3).text("Данные экспедитор"))
                .append($("<th class='jsgrid-header-row' style='text-align:center; background-color:#999999; font:14px Arial, sans-serif;'>").attr("colspan",2).text("Прибытие ТС"))

                .append($("<th class='jsgrid-header-row' style='text-align:center; background-color:#f9cb9c; font:14px Arial, sans-serif;'>").attr("colspan",2).text("Нетто"))
                .append($("<th class='jsgrid-header-row' style='text-align:center; background-color:#f9cb9c; font:14px Arial, sans-serif;'>").text("Состояние"))
                .append($("<th class='jsgrid-header-row' style='text-align:center; background-color:#f9cb9c; font:14px Arial, sans-serif;'>").text("Примечание"))
                .append($("<th class='jsgrid-header-row' style='text-align:center; background-color:#f9cb9c; font:14px Arial, sans-serif;'>").attr("colspan",2).text("Убытие ТС"))
            )

            return $result.add($("<tr class='jsgrid-header-row'>")
                .append($("<th class='jsgrid-header-cell'>").text("Заявка ПР"))
                .append($("<th class='jsgrid-header-cell'>").text("Дата"))
                .append($("<th class='jsgrid-header-cell' >").text("Время"))
                .append($("<th class='jsgrid-header-cell'>").text("Условия"))
                .append($("<th class='jsgrid-header-cell'>").text("Адрес"))
                .append($("<th class='jsgrid-header-cell' >").text("Покупатель/плательщик"))
                .append($("<th class='jsgrid-header-cell'>").text("Грузополучатель"))
                .append($("<th class='jsgrid-header-cell'>").text("Заказ ТС"))
                .append($("<th class='jsgrid-header-cell'>").text("ТН"))
                .append($("<th class='jsgrid-header-cell'>").text("Лукоил"))
                .append($("<th class='jsgrid-header-cell'>").text("ПБВ"))
                .append($("<th class='jsgrid-header-cell' style='text-align:center; background-color:#e06666;'>").text("Дата"))
                .append($("<th class='jsgrid-header-cell' style='text-align:center; background-color:#e06666;'>").text("Время"))
                .append($("<th class='jsgrid-header-cell' style='text-align:center; background-color:#e06666;'>").text("+Часов"))
                .append($("<th class='jsgrid-header-cell' style='text-align:center; background-color:#999999;'>").text("№ТС"))
                .append($("<th class='jsgrid-header-cell' style='text-align:center; background-color:#999999;'>").text("Водитель"))
                .append($("<th class='jsgrid-header-cell' style='text-align:center; background-color:#999999;'>").text("Экспедитор"))
                .append($("<th class='jsgrid-header-cell' style='text-align:center; background-color:#999999;'>").text("Дата"))
                .append($("<th class='jsgrid-header-cell' style='text-align:center; background-color:#999999;'>").text("Время"))

                .append($("<th class='jsgrid-header-cell' style='text-align:center; background-color:#f9cb9c;'>").text("ПБВ60"))
                .append($("<th class='jsgrid-header-cell' style='text-align:center; background-color:#f9cb9c;'>").text("ПБВ90"))
                .append($("<th class='jsgrid-header-cell' style='text-align:center; background-color:#f9cb9c;'>").text("Статус ТС"))
                .append($("<th class='jsgrid-header-cell' style='text-align:center; background-color:#f9cb9c;'>").text(" "))
                .append($("<th class='jsgrid-header-cell' style='text-align:center; background-color:#f9cb9c;'>").text("Дата"))
                .append($("<th class='jsgrid-header-cell' style='text-align:center; background-color:#f9cb9c;'>").text("Время"))

            );

        },


        fields:
            <?=CUtil::PhpToJSObject($fields)?>
            
            // { name: "Заявка ПР", type: "text",validate: "required" },
            // { name: "Дата заявки", type: "text" },
            // { name: "Время", type: "text"},
            // { name: "Условия", type: "text"},
            // { name: "Адрес", type: "text"},
            // { name: "Покупатель/плательщик", type: "text"},
            // { name: "Грузополучатель", type: "text"},
            // { name: "Заказ ТС", type: "text"},
            // { name: "ТН", type: "text"},
            // { name: "Лукоил", type: "text"},
            // { name: "ПБВ", type: "text"},
            // { name: "Дата", type: "text"},
            // { name: "Время_Н", type: "text"},
            // { name: "+Часов", type: "text"},
            // { name: "№ТС", type: "text"},
            // { name: "Водитель", type: "text"},
            // { name: "Экспедитор", type: "text"},
            // { name: "Дата_Л", type: "text"},
            // { name: "Время_Л", type: "text"},
            // { name: "ПБВ60", type: "text"},
            // { name: "ПБВ90", type: "text"},
            // { name: "СТАТУС ТС", type: "text"},
            // { name: "comments", type: "text"},
            // { name: "Дата_СЛ", type: "text"},
            // { name: "Время_СЛ", type: "text"}


    });
    if (!window.DOMTokenList) {
        Element.prototype.containsClass = function(name) {
            return new RegExp("(?:^|\\s+)" + name + "(?:\\s+|$)").test(this.className);
        };
        Element.prototype.addClass = function(name) {
            if (!this.containsClass(name)) {
                var c = this.className;
                this.className = c ? [c, name].join(' ') : name;
            }
        };
        Element.prototype.removeClass = function(name) {
            if (this.containsClass(name)) {
                var c = this.className;
                this.className = c.replace(
                    new RegExp("(?:^|\\s+)" + name + "(?:\\s+|$)", "g"), "");
            }
        };
    }

    // sse.php sends messages with text/event-stream mimetype.
    var source = new EventSource('/local/components/biotum.manufacture/bid/sse.php');
    function Logger(id) {
        this.el = document.getElementById(id);
    }
    Logger.prototype.log = function(msg, opt_class) {
        var fragment = document.createDocumentFragment();
        var p = document.createElement('p');
        p.className = opt_class || 'info';
        p.textContent = msg;
        fragment.appendChild(p);
        this.el.appendChild(fragment);
    };
    Logger.prototype.clear = function() {
        this.el.textContent = '';
    };
    var logger = new Logger('log');
    function closeConnection() {
        source.close();
        logger.log('> Connection was closed');
        updateConnectionStatus('Disconnected', false);
    }
    function updateConnectionStatus(msg, connected) {
        var el = document.querySelector('#connection');
        if (connected) {
            if (el.classList) {
                el.classList.add('connected');
                el.classList.remove('disconnected');
            } else {
                el.addClass('connected');
                el.removeClass('disconnected');
            }
        } else {
            if (el.classList) {
                el.classList.remove('connected');
                el.classList.add('disconnected');
            } else {
                el.removeClass('connected');
                el.addClass('disconnected');
            }
        }
        el.innerHTML = msg + '<div></div>';
    }
    source.addEventListener('message', function(event) {

        var data = JSON.parse(event.data);
        var d = new Date(data.msg * 1e3);
        var timeStr = [d.getHours(), d.getMinutes(), d.getSeconds()].join(':');
     //   coolclock.render(d.getHours(), d.getMinutes(), d.getSeconds());
        logger.log('lastEventID: ' + event.lastEventId +
            ', server time: ' + timeStr, 'msg');
        $("#jsGrid").jsGrid("loadData").done(function() {
            logger.log("rendering completed and data loaded");
        });
    }, false);
    source.addEventListener('open', function(event) {
        logger.log('> Connection was opened');
        updateConnectionStatus('Connected', true);
    }, false);
    source.addEventListener('error', function(event) {
        if (event.eventPhase == 2) { //EventSource.CLOSED
            logger.log('> Connection was closed');
            updateConnectionStatus('Disconnected', false);
        }
    }, false);
 //   var coolclock = CoolClock.findAndCreateClocks();
</script>

<script type="text/javascript">
    function openRecallPopup()
    {
        var authPopup = BX.PopupWindowManager.create("RecallPopup", null, {
            autoHide: false,
            offsetLeft: 0,
            offsetTop: 0,
            overlay : true,
            draggable: {restrict:true},
            closeByEsc: true,
            closeIcon: { right : "12px", top : "10px"},
            content: '<div style="width: 96%; margin: 10px 2%; text-align: center;"></div>',
            events: {
                onAfterPopupShow: function()
                {
                    this.setContent(BX("bx_recall_popup_form"));
                }
            }
        });

        authPopup.show();
        $("#jsGrid").jsGrid("refresh").done(function() {
            logger.log("rendering completed and data loaded");
        });
    }
</script>





