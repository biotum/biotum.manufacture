<?php
defined('B_PROLOG_INCLUDED') || die;

$MESS['CRMBIDS_LIST_TITLE'] = 'Заявки на производство';
$MESS['CRMBIDS_ADD'] = 'Добавить';
$MESS['CRMSTORES_CONFIGURE_WORKFLOWS'] = 'Настроить бизнес-процессы';