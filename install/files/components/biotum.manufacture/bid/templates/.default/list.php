<?php
defined('B_PROLOG_INCLUDED') || die;

use Bitrix\Main\Localization\Loc;


/** @var CBitrixComponentTemplate $this */

$APPLICATION->SetTitle(Loc::getMessage('CRMBIDS_LIST_TITLE'));

$APPLICATION->IncludeComponent(
    'bitrix:crm.control_panel',
    '',
    array(
        'ID' => 'BID',
        'ACTIVE_ITEM_ID' => 'BID',
    ),
    $component
);

$urlTemplates = array(
    'DETAIL' => $arResult['SEF_FOLDER'] . $arResult['SEF_URL_TEMPLATES']['details'],
    'EDIT' => $arResult['SEF_FOLDER'] . $arResult['SEF_URL_TEMPLATES']['edit'],
    'BP_LIST' => $arResult['SEF_FOLDER'] . $arResult['SEF_URL_TEMPLATES']['bizproc_workflow_admin'],
);

$APPLICATION->IncludeComponent(
    'bitrix:crm.interface.toolbar',
    'title',
    array(
        'TOOLBAR_ID' => 'CRMBIDS_TOOLBAR',
        'BUTTONS' => array(
//            array(
//                'TEXT' => Loc::getMessage('CRMBIDS_ADD'),
//                'TITLE' => Loc::getMessage('CRMBIDS_ADD'),
//                'LINK' => CComponentEngine::makePathFromTemplate($urlTemplates['EDIT'], array('BID_ID' => 0)),
//                'ICON' => 'btn-add',
//            ),
            array('NEWBAR' => true),
            array(
                'TEXT' => Loc::getMessage('CRMSTORES_CONFIGURE_WORKFLOWS'),
                'TITLE' => Loc::getMessage('CRMSTORES_CONFIGURE_WORKFLOWS'),
                'LINK' => $urlTemplates['BP_LIST']
            )
        )
    ),
    $this->getComponent(),
    array('HIDE_ICONS' => 'Y')
);

$APPLICATION->IncludeComponent(
    'biotum.manufacture:bid.list',
    '',
    array(
        'URL_TEMPLATES' => $urlTemplates,
        'SEF_FOLDER' => $arResult['SEF_FOLDER'],
    ),
    $this->getComponent(),
    array('HIDE_ICONS' => 'Y',)
);