<?php
defined('B_PROLOG_INCLUDED') || die;

/** @var CBitrixComponentTemplate $this */
use Bitrix\Main\Localization\Loc;
$APPLICATION->IncludeComponent(
    'bitrix:crm.control_panel',
    '',
    array(
        'ID' => 'BID',
        'ACTIVE_ITEM_ID' => 'BID',
    ),
    $component
);

$urlTemplates = array(
    'DETAIL' => $arResult['SEF_FOLDER'] . $arResult['SEF_URL_TEMPLATES']['details'],
    'EDIT' => $arResult['SEF_FOLDER'] . $arResult['SEF_URL_TEMPLATES']['edit'],
);
$editUrl = CComponentEngine::makePathFromTemplate(
    $urlTemplates['EDIT'],
    array('BID_ID' => $arResult['VARIABLES']['BID_ID'])
);

$viewUrl = CComponentEngine::makePathFromTemplate(
    $urlTemplates['DETAIL'],
    array('BID_ID' => $arResult['VARIABLES']['BID_ID'])
);

$viewUrl = new \Bitrix\Main\Web\Uri($viewUrl);
//$editUrl->addParams(array('backurl' => $viewUrl));

$APPLICATION->IncludeComponent(
    'bitrix:crm.interface.toolbar',
    'type2',
    array(
        'TOOLBAR_ID' => 'CRMBIDS_TOOLBAR',
        'BUTTONS' => array(
            array(
                'TEXT' => Loc::getMessage('CRMBID_SHOW'),
                'TITLE' => Loc::getMessage('CRMBID_SHOW'),
                'LINK' => $viewUrl->getUri(),
                'ICON' => 'btn-edit',
            ),

        )
    ),
    $this->getComponent(),
    array('HIDE_ICONS' => 'Y')
);

$APPLICATION->IncludeComponent(
    'biotum.manufacture:bid.edit',
    '',
    array(
        'BID_ID' => $arResult['VARIABLES']['BID_ID'],
        'URL_TEMPLATES' => $urlTemplates,
        'SEF_FOLDER' => $arResult['SEF_FOLDER'],
    ),
    $this->getComponent(),
    array('HIDE_ICONS' => 'Y',)
);



