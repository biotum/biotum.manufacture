<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

CPullWatch::Add($USER->GetId(), 'PULL_TEST');

?>
<button onclick="sendCommand();">It`s work!</button>

<div id="PULL_TEST"></div>

<script type="text/javascript">

function sendCommand()
{
	BX.ajax({
		url: '/local/components/biotum.manufacture/bid.pull/ajax.php',
		method: 'POST',
		data: {'SEND' : 'Y', 'sessid': BX.bitrix_sessid()}
	});
}

BX.ready(function(){
	BX.addCustomEvent("onPullEvent", function(module_id,command,params) {
		console.log(module_id,command,params);
		if (module_id == "biotum.manufacture" && command == 'check')
		{
			BX('PULL_TEST').innerHTML += params.TIME+'<br>';
		}
	});
	BX.PULL.extendWatch('PULL_TEST');
});
</script>
