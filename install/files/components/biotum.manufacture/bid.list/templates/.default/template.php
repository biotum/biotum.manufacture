<?php
defined('B_PROLOG_INCLUDED') || die;

use Bitrix\Crm\CompanyTable;
use Bitrix\Main\Config\Option;
use Bitrix\Main\Grid\Panel\Snippet;
use Bitrix\Main\Loader;
use Bitrix\Main\Localization\Loc;
use Bitrix\Main\Page\Asset;
use Bitrix\Main\Web\Json;
use Academy\CrmStores\Entity\DeliveryProductTable;

function getStoreName($id)
{
    $store = \Academy\CrmStores\Entity\StoreTable::getById((int) $id);
    if(!$result = $store->fetch()){
        return false;
    }
    return $result;
}

function getStoreTimeTravel($id)
{
    $store = \Academy\CrmStores\Entity\StoreTable::getById($id);
    $result = $store->fetch();
    return $result[0]['TRAVEL_TIME'];
}

function getStyleStatusBid($id)
{

    $status = json_decode(COption::GetOptionString('biotum.manufacture', 'APPEAL_FIELD_DESTINATION'), true);
    return '<span class="crm-widget-content-text" style="font-size:12px; color:white; padding:4px;  text-transform: uppercase; background:' . $status[$id][1] . ';">' . $status[$id][0] . '</span>';
}


function getDeliveryProduct($id)
{
      $delivery = DeliveryProductTable::getList(
        [
            'filter' => ['ID' => $id]
        ]);
    $result = $delivery->fetch();



//    if (!empty($result)) {
//        $html .= '<div style="border: 1px solid black;">
//    <table class="table">
//            <tbody><tr>
//                <th>Пункт разгрузки</th>
//                <th>Время в пути (часов)</th>
//                <th>Вес</th>
//                <th>Дата поставки</th>
//                <th>Время поставки</th>
//            </tr>
//            <tr class="">
//
//            </tr>';
//
//        foreach ($result as $value) {
//
//            $html .= '<tr class="">
//                    <td class="" style="min-width: 400px;">
//                       <span class="" data-prevent-default="true"> <a class="feed-com-add-link" href="/crm/stores/' . $value['STORE_ID'] . '/">' . getStoreName($value['STORE_ID']) . '</a></span>
//                    </td>
//                    <td class=""  ><span class="main-grid-cell-content" data-prevent-default="true">' . getStoreTimeTravel($value['STORE_ID']) .'</span></td>
//                    <td class=""  ><span class="main-grid-cell-content" data-prevent-default="true">' . $value['WEIGHT'] . '</span></td>
//                    <td class="" style="min-width: 100px;" ><span class="main-grid-cell-content" data-prevent-default="true">' . $value['DATE_DELIVERY'] . '</span></td>
//                    <td class=""  ><span class="" data-prevent-default="true">' . $value['TIME_DELIVERY'] . '</span></td>
//                </tr>';
//        }
//        $html .= '</tbody></table></div>';
//    }
    return $result;
}


/** @var CBitrixComponentTemplate $this */

if (!Loader::includeModule('crm')) {
    ShowError(Loc::getMessage('CRMBIDS_NO_CRM_MODULE'));
    return;
}

$asset = Asset::getInstance();
$asset->addJs('/bitrix/js/crm/interface_grid.js');

$gridManagerId = $arResult['GRID_ID'] . '_MANAGER';

$rows = array();
foreach ($arResult['BIDS'] as $store) {

    $viewUrl = CComponentEngine::makePathFromTemplate(
        $arParams['URL_TEMPLATES']['DETAIL'],
        array('BID_ID' => $store['ID'])
    );
    $editUrl = CComponentEngine::makePathFromTemplate(
        $arParams['URL_TEMPLATES']['EDIT'],
        array('BID_ID' => $store['ID'])
    );

    $deleteUrlParams = http_build_query(array(
        'action_button_' . $arResult['GRID_ID'] => 'delete',
        'ID' => array($store['ID']),
        'sessid' => bitrix_sessid()
    ));
    $deleteUrl = $arParams['SEF_FOLDER'] . '?' . $deleteUrlParams;
    $delivery=getDeliveryProduct($store['DELIVERY_ID']);
    $stores=getStoreName($delivery['STORE_ID']);

    $rows[] = array(
        'id' => $store['ID'],
        'actions' => array(
            array(
                'TITLE' => Loc::getMessage('CRMBIDS_ACTION_VIEW_TITLE'),
                'TEXT' => Loc::getMessage('CRMBIDS_ACTION_VIEW_TEXT'),
                'ONCLICK' => 'BX.Crm.Page.open(' . Json::encode($viewUrl) . ')',
                'DEFAULT' => true
            ),
            array(
                'TITLE' => Loc::getMessage('CRMBIDS_ACTION_EDIT_TITLE'),
                'TEXT' => Loc::getMessage('CRMBIDS_ACTION_EDIT_TEXT'),
                'ONCLICK' => 'BX.Crm.Page.open(' . Json::encode($editUrl) . ')',
            ),
        ),
        'data' => $store,
        'columns' => array(
            'ID' => $store['ID'],
            'NUMBER_BID' => '<a href="' . $viewUrl . '" target="_self"> №' . $store['NUMBER_BID'] . ' от ' . $store['DATE_BID'] . '</a>',
            'STATUS_BID' => getStyleStatusBid($store['STATUS_BID']),
            'PRODUCT_ID' =>'<div>'.$store['PRODUCT_NAME'].'</div><strong>Время производства '.$store['PRODUCT_TIME_MANUFACTURE'].' часов </strong>' ,
            'DATETIME_START_MANUFACTURE' => '<div> Дата </div><strong>' .ConvertDateTime($store['DATETIME_START_MANUFACTURE'],'DD-MM-YYYY','ru'). '</strong><div> Время</div><strong>' .ConvertDateTime($store['DATETIME_START_MANUFACTURE'], "HH:MI", "ru"). '</strong>',
            'DATETIME_SHIPMENTREADY' => '<div> Дата </div><strong>' .ConvertDateTime($store['DATETIME_SHIPMENTREADY'],'DD-MM-YYYY','ru'). '</strong><div> Время </div><strong>' .ConvertDateTime($store['DATETIME_SHIPMENTREADY'], "HH:MI", "ru"). '</strong>',
           // 'DEAL_ID' =>$delivery['STORE_ID'],
            'STORE'=>$stores['ADDRESS'],
            'WEIGHT_DELIVERY'=>$delivery['WEIGHT'],
            'TRAVEL_TIME'=>$stores['TRAVEL_TIME'],
            'DATE_DELIVERY'=>'<div> Дата </div><strong>' .ConvertDateTime($delivery['DATE_DELIVERY'],'DD-MM-YYYY','ru'). '</strong><div> Время</div><strong>' .ConvertDateTime($delivery['DATE_DELIVERY'], "HH:MI", "ru"). '</strong>',
            'BUYER_ID' => $store['COMPANY_NAME'],
            'CONSIGNEE_ID' => $store['CONSIGNEE_NAME'],
            //'WEIGHT' => $store['WEIGHT'],
            'COMMENT' => $store['COMMENT'],
            'ASSIGNED_BY' => empty($store['ASSIGNED_BY']) ? '' : CCrmViewHelper::PrepareUserBaloonHtml(
                array(
                    'PREFIX' => "BID_{$store['ID']}_RESPONSIBLE",
                    'USER_ID' => $store['ASSIGNED_BY_ID'],
                    'USER_NAME' => CUser::FormatName(CSite::GetNameFormat(), $store['ASSIGNED_BY']),
                    'USER_PROFILE_URL' => Option::get('intranet', 'path_user', '', SITE_ID) . '/'
                )
            ),

        )
    );
}

$snippet = new Snippet();

$APPLICATION->IncludeComponent(
    'bitrix:crm.interface.grid',
    'titleflex',
    array(
        'GRID_ID' => $arResult['GRID_ID'],
        'HEADERS' => $arResult['HEADERS'],
        'ROWS' => $rows,
        'PAGINATION' => $arResult['PAGINATION'],
        'SORT' => $arResult['SORT'],
        'FILTER' => $arResult['FILTER'],
        'FILTER_PRESETS' => $arResult['FILTER_PRESETS'],
        'IS_EXTERNAL_FILTER' => false,
        'ENABLE_LIVE_SEARCH' => $arResult['ENABLE_LIVE_SEARCH'],
        'DISABLE_SEARCH' => $arResult['DISABLE_SEARCH'],
        'ENABLE_ROW_COUNT_LOADER' => true,
        'AJAX_ID' => '',
        'AJAX_OPTION_JUMP' => 'N',
        'AJAX_OPTION_HISTORY' => 'N',
        'AJAX_LOADER' => null,
        'ACTION_PANEL' => array(
            'GROUPS' => array(
                array(
                    'ITEMS' => array(
                        $snippet->getRemoveButton(),
                        $snippet->getForAllCheckbox(),
                    )
                )
            )
        ),
        'EXTENSION' => array(
            'ID' => $gridManagerId,
            'CONFIG' => array(
                'ownerTypeName' => 'BIDS',
                'gridId' => $arResult['GRID_ID'],
                'serviceUrl' => $arResult['SERVICE_URL'],
            ),
            'MESSAGES' => array(
                'deletionDialogTitle' => Loc::getMessage('CRMBIDS_DELETE_DIALOG_TITLE'),
                'deletionDialogMessage' => Loc::getMessage('CRMBIDS_DELETE_DIALOG_MESSAGE'),
                'deletionDialogButtonTitle' => Loc::getMessage('CRMBIDS_DELETE_DIALOG_BUTTON'),
            )
        ),
    ),
    $this->getComponent(),
    array('HIDE_ICONS' => 'Y',)
);

$APPLICATION->IncludeComponent("biotum:pull.test", '');