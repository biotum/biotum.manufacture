<?php
defined('B_PROLOG_INCLUDED') || die;

$MESS['CRMSTORES_NO_CURRENCY_MODULE'] = 'Не установлен модуль валют.';
$MESS['CRMBIDS_DEAL_ID'] = 'ID';
$MESS['CRMBIDS_DEAL_TITLE'] = 'Название Сделки';
$MESS['CRMBIDS_DEAL_OPPORTUNITY'] = 'Сумма ';
$MESS['CRMBIDS_COMPANY_ID'] = 'ID';
$MESS['CRMBIDS_COMPANY_TITLE'] = 'Название Компании';
$MESS['CRMBIDS_NAME_TITLE']= 'Пункт разгрузки';
$MESS['CRMBIDS_PRODUCT_TITLE'] ='Наименование продукта';
$MESS['CRMBIDS_WEIGHT_TITLE'] ='Вес продукта';
$MESS['CRMBIDS_DATE_DELIVERY_TITLE'] ='Срок доставки грузополучателю (S)';
$MESS['CRMBIDS_TRAVEL_TIME'] ='Рассчетное время в пути (x) (часов)';
$MESS['CRMBIDS_TIME_DELIVERY_TITLE'] ='Время доставки';




