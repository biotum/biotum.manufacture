<?php
defined('B_PROLOG_INCLUDED') || die;

use Bitrix\Main\Config\Option;
use Bitrix\Main\Loader;
use Bitrix\Main\Localization\Loc;

global $APPLICATION;

/** @var CBitrixComponentTemplate $this */

if (!Loader::includeModule('biotum.manufacture')) {
    ShowError(Loc::getMessage('CRMBIDS_NO_CURRENCY_MODULE'));
    return;
}

$bodyClass = $APPLICATION->GetPageProperty('BodyClass');
$APPLICATION->SetPageProperty('BodyClass', ($bodyClass ? $bodyClass . ' ' : '') . 'disable-grid-mode');
$rows = array();
foreach ($arResult['DEALS'] as $deal) {
    $dealUrl = CComponentEngine::makePathFromTemplate(
        Option::get('academy.crmstores', 'DEAL_DETAIL_TEMPLATE'),
        array('DEAL_ID' => $deal['DEAL_ID'])
    );

    $dbStore = \Academy\CrmStores\Entity\StoreTable::getById($deal['STORE_ID']);

    $store = $dbStore->fetch();
    $dealID = CCrmDeal::GetByID($deal['DEAL_ID']);
    $storeUrl = CComponentEngine::makePathFromTemplate(
        Option::get('academy.crmstores', 'STORE_DETAIL_TEMPLATE'),
        array('STORE_ID' => $store['ID'])
    );
    $rows[] = array(
        'id' => $deal['STORE_ID'],
        'columns' => array(
            'ID' => $deal['STORE_ID'],
            'TITLE' => '<a href="' . htmlspecialcharsbx($dealUrl) . '">' .$dealID['TITLE'].'</br> от '.$dealID['DATE_CREATE'].'</a>',
            'CONSIGNEE'=> '<a href="' . htmlspecialcharsbx($storeUrl) . '">'.$store['NAME'].'</a>',
            'TRAVEL_TIME'=> $store['TRAVEL_TIME'],
            'WEIGHT'=>$deal['WEIGHT'],
            'DATE_DELIVERY'=> 'Дата->'.ConvertDateTime($deal['DATE_DELIVERY'],'DD-MM-YYYY','ru').' Время->'.ConvertDateTime($deal['DATE_DELIVERY'], "HH:MI", "ru"),

        )
    );
}

$APPLICATION->IncludeComponent(
    'bitrix:main.ui.grid',
    '',
    array(
        'GRID_ID' => 'CRMSTORES_BOUND_DEALS',
        'HEADERS' => array(
            array(
                'id' => 'ID',
                'name' => Loc::getMessage('CRMBIDS_DEAL_ID'),
                'type' => 'int',
                'default' => false,
            ),
            array(
                'id' => 'TITLE',
                'name' => Loc::getMessage('CRMBIDS_DEAL_TITLE'),
                'default' => true
            ),
            array(
                'id' => 'CONSIGNEE',
                'name' => Loc::getMessage('CRMBIDS_NAME_TITLE'),
                'default' => true
            ),
            array(
                'id' => 'TRAVEL_TIME',
                'name' => Loc::getMessage('CRMBIDS_TRAVEL_TIME'),
                'default' => true
            ),
              array(
                'id' => 'WEIGHT',
                'name' => Loc::getMessage('CRMBIDS_WEIGHT_TITLE'),
                'default' => true
            ),
            array(
                'id' => 'DATE_DELIVERY',
                'name' => Loc::getMessage('CRMBIDS_DATE_DELIVERY_TITLE'),
                'default' => true
            ),
//            array(
//                'id' => 'TIME_DELIVERY',
//                'name' => Loc::getMessage('CRMBIDS_TIME_DELIVERY_TITLE'),
//                'default' => true
//            ),

        ),
        'ROWS' => $rows
    ),
    $this->getComponent(),
    array('HIDE_ICONS' => 'Y')
);


