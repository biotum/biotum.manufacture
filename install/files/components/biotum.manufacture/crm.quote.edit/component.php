<?php
use Bitrix\Crm\Integration\StorageManager;
use Bitrix\Crm\Integration\StorageType;
use \Bitrix\Crm\Binding\EntityBinding;

if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true)die();

if (!CModule::IncludeModule('biotum.manufacture'))
{
	ShowError(GetMessage('CRM_MODULE_NOT_INSTALLED'));
	return;
}

CModule::IncludeModule('fileman');
$CCrmQuote = new CCrmQuote();




global $USER_FIELD_MANAGER, $DB, $USER;

//$CCrmUserType = new CCrmUserType($USER_FIELD_MANAGER, CCrmBid::$sUFEntityID);
$userPermissions = CCrmPerms::GetCurrentUserPermissions();

$isNew = $arParams['ELEMENT_ID'] <= 0;
$bEdit = false;
$bCopy = false;
$bVarsFromForm = false;
$arParams['ELEMENT_ID'] = (int) $arParams['ELEMENT_ID'];
if (!empty($arParams['ELEMENT_ID']))
	$bEdit = true;
if (!empty($_REQUEST['copy']))
{
	$bCopy = true;
	$bEdit = false;
}

$arPersonTypes = $arResult['PERSON_TYPE_IDS'] = CCrmPaySystem::getPersonTypeIDs();

$requisiteIdLinked = 0;
$bankDetailIdLinked = 0;
$mcRequisiteIdLinked = 0;
$mcBankDetailIdLinked = 0;

$arFields = null;
if ($conversionWizard !== null)
{
	$arFields = array('ID' => 0);
	if($_SERVER['REQUEST_METHOD'] === 'GET')
	{
		$conversionWizard->prepareDataForEdit(CCrmOwnerType::System, $arFields, true);
		if(isset($arFields['PRODUCT_ROWS']))
		{
			$arResult['PRODUCT_ROWS'] = $arFields['PRODUCT_ROWS'];
		}
	}
	$arResult['CONVERSION_LEGEND'] = $conversionWizard->getEditFormLegend();
}

elseif ($bEdit || $bCopy)
{
	$arFilter = array(
		'ID' => $arParams['ELEMENT_ID'],
		'PERMISSION' => 'WRITE'
	);
//	$obFields = CCrmBid::GetList(array(), $arFilter);
//	$arFields = $obFields->GetNext();
//print_r($arFields);
	if ($arFields === false)
	{
		$bEdit = false;
		$bCopy = false;
	}
	//else
	//	$arEntityAttr = $CCrmQuote->cPerms->GetEntityAttr('SYSTEM', array($arParams['ELEMENT_ID']));

}
else
{
	$arFields = array(
		'ID' => 0
	);

}


// storage type
//$storageTypeId = isset($arFields['STORAGE_TYPE_ID'])
//	? (int)$arFields['STORAGE_TYPE_ID'] : CCrmQuoteStorageType::Undefined;
//if($storageTypeId === CCrmQuoteStorageType::Undefined
//	|| !CCrmQuoteStorageType::IsDefined($storageTypeId))
//{
	$storageTypeId = CCrmBid::GetDefaultStorageTypeID();
//}

$arFields['STORAGE_TYPE_ID'] = $arFields['~STORAGE_TYPE_ID'] = $storageTypeId;
$arResult['ENABLE_DISK'] = $storageTypeId === StorageType::Disk;
$arResult['ENABLE_WEBDAV'] = $storageTypeId === StorageType::WebDav;

// storage elements
CCrmBid::PrepareStorageElementIDs($arFields);


// Determine person type
//$personTypeId = is_array($arFields) ? CCrmBid::ResolvePersonType($arFields, $arPersonTypes) : 0;
//$arResult['ELEMENT'] = is_array($arFields) ? $arFields : null;
//unset($arFields);
//
$arResult['FORM_ID'] = !empty($arParams['FORM_ID']) ? $arParams['FORM_ID'] : 'CRM_BID_EDIT_V12';
$arResult['GRID_ID'] = 'CRM_BID_LIST_V12';
$arResult['FILES_FIELD_CONTAINER_ID'] = $arResult['FORM_ID'].'_FILES_CONTAINER';
$arResult['FORM_CUSTOM_HTML'] = '';


$sProductsHtml = '';

ob_start();

$APPLICATION->IncludeComponent('bitrix:crm.product_row.list',
	'',
	'',
	false,
	array('HIDE_ICONS' => 'Y', 'ACTIVE_COMPONENT'=>'Y')
);
$sProductsHtml .= ob_get_contents();
ob_end_clean();
unset($componentSetting);


$icnt = count($arResult['FIELDS']['tab_1']);

if($conversionWizard !== null)
{
	$useUserFieldsFromForm = true;

}
else
{
	$useUserFieldsFromForm = $bVarsFromForm;

}



$this->IncludeComponentTemplate();

?>
