<?php
defined('B_PROLOG_INCLUDED') || die;

use Biotum\Manufacture\Entity\BidsTable;
use Bitrix\Main\Context;
use Bitrix\Main\Entity\Event;
use Bitrix\Main\Entity\EventResult;
use Bitrix\Main\Error;
use Bitrix\Main\ErrorCollection;
use Bitrix\Main\Loader;
use Bitrix\Main\Localization\Loc;
use Bitrix\Main\UserTable;
use Bitrix\Crm\Integration\StorageType;
class CBiotumManufactureBidsEditComponent extends CBitrixComponent
{
    const FORM_ID = 'CRMBIDS_EDIT';

    private $errors;

    public function __construct(CBitrixComponent $component = null)
    {
        parent::__construct($component);

        $this->errors = new ErrorCollection();

        CBitrixComponent::includeComponentClass('biotum.manufacture:bid.list');
      //  CModule::IncludeModule('fileman');
    }

    public function executeComponent()
    {
        global $APPLICATION,$USER_FIELD_MANAGER, $DB, $USER;

        $title = Loc::getMessage('CRMBID_SHOW_TITLE_DEFAULT');

        if (!Loader::includeModule('biotum.manufacture')) {
            ShowError(Loc::getMessage('CRMBID_NO_MODULE'));
            return;
        }

        $arParams['ELEMENT_ID'] = (int) $this->arParams['BID_ID'];
        if (!empty($arParams['ELEMENT_ID']))
            $bEdit = true;
        if (!empty($_REQUEST['copy']))
        {
            $bCopy = true;
            $bEdit = false;
        }

        $arPersonTypes = $arResult['PERSON_TYPE_IDS'] = CCrmPaySystem::getPersonTypeIDs();

        $requisiteIdLinked = 0;
        $bankDetailIdLinked = 0;
        $mcRequisiteIdLinked = 0;
        $mcBankDetailIdLinked = 0;

        $arFields = null;


// Determine person type

        $arResult['FORM_ID'] = !empty($arParams['FORM_ID']) ? $arParams['FORM_ID'] : 'CRM_BID_EDIT_V12';
        $arResult['GRID_ID'] = 'CRM_BID_LIST_V12';
        $arResult['FILES_FIELD_CONTAINER_ID'] = $arResult['FORM_ID'].'_FILES_CONTAINER';
        $arResult['FORM_CUSTOM_HTML'] = '';
//
//
// FILES
        if($arResult['ENABLE_WEBDAV'] || $arResult['ENABLE_DISK'])
        {
            $sVal = '<div id="'.$arResult['FILES_FIELD_CONTAINER_ID'].'" class="bx-crm-dialog-activity-webdav-container"></div>';
        }
        else
        {
            ob_start();

            $sVal = ob_get_contents();
            ob_end_clean();
            $sVal = '<div id="'.$arResult['FILES_FIELD_CONTAINER_ID'].'" class="bx-crm-dialog-activity-webdav-container">'.
                '<div id="'.$arResult['PREFIX'].'_upload_container"'.
                ($arResult['ENABLE_WEBDAV'] ? ' style="display:none;"' : '').'>'.$sVal.'</div></div>';
        }

        if(!$arResult['ENABLE_WEBDAV'])
        {
            $arResult['WEBDAV_SELECT_URL'] = $arResult['WEBDAV_UPLOAD_URL'] = $arResult['WEBDAV_SHOW_URL'] = '';
        }
        else
        {
            $webDavPaths = CCrmWebDavHelper::GetPaths();
            $arResult['WEBDAV_SELECT_URL'] = isset($webDavPaths['PATH_TO_FILES'])
                ? $webDavPaths['PATH_TO_FILES'] : '';
            $arResult['WEBDAV_UPLOAD_URL'] = isset($webDavPaths['ELEMENT_UPLOAD_URL'])
                ? $webDavPaths['ELEMENT_UPLOAD_URL'] : '';
            $arResult['WEBDAV_SHOW_URL'] = isset($webDavPaths['ELEMENT_SHOW_INLINE_URL'])
                ? $webDavPaths['ELEMENT_SHOW_INLINE_URL'] : '';
        }
/// /////



        if (intval($this->arParams['BID_ID']) > 0) {
            $dbStore = BidsTable::getById($this->arParams['BID_ID']);
            $store = $dbStore->fetch();

            if (empty($store)) {
                ShowError(Loc::getMessage('CRMBID_BID_NOT_FOUND'));
                return;
            }
        }

        if (!empty($store['ID'])) {
            $title = Loc::getMessage(
                'CRMBID_SHOW_TITLE',
                array(
                    '#ID#' => $store['ID'],
                    '#NUMBER_BID#' => $store['NUMBER_BID'],
                    '#COMMENT#' => $store['COMMENT'],
//                    '#PHONE#' => $store['PHONE'],
//                    '#EMAIL#' => $store['EMAIL'],
                )
            );
        }

        $APPLICATION->SetTitle($title);

        if (self::isFormSubmitted()) {
            $savedStoreId = $this->processSave($store);
            if ($savedStoreId > 0) {
                LocalRedirect($this->getRedirectUrl($savedStoreId));
            }

            $submittedStore = $this->getSubmittedStore();
            $store = array_merge($store, $submittedStore);
        }

        $this->arResult =array(
            'FORM_ID' => self::FORM_ID,
            'GRID_ID' => CBiotumManufactureBidsListComponent::GRID_ID,
            'IS_NEW' => empty($store['ID']),
            'TITLE' => $title,
            'BIDS' => \Biotum\Manufacture\Entity\ManufacturreBids::getlistID(['filter'=>['ID'=>$this->arParams['BID_ID']]]),
            'STORAGE'=> $sVal,
            'ELEMENT'=>$arResult['ELEMENT'],
            'ELEMENT_ID'=>$arParams['ELEMENT_ID'],
            'BACK_URL' => $this->getRedirectUrl(),
            'ERRORS' => $this->errors,
        );
//print_r( $this->arResult);
//exit;
        $this->includeComponentTemplate();
    }

    private function processSave($initialStore)
    {
        $submittedStore = $this->getSubmittedStore();
//print_r($submittedStore);
//exit;
        $store = array_merge($initialStore, $submittedStore);

        $this->errors = self::validate($store);

        if (!$this->errors->isEmpty()) {
            return false;
        }

        if (!empty($store['ID'])) {
            $result = BidsTable::update($store['ID'], $store);

        } else {
            $result = BidsTable::add($store);

        }

        if (!$result->isSuccess()) {
            $this->errors->add($result->getErrors());
        }

        return $result->isSuccess() ? $result->getId() : false;
    }



    private function getSubmittedStore()
    {
        $context = Context::getCurrent();
        $request = $context->getRequest();

        $submittedStore = array(
         //   'NUMBER_BID' => $request->get('NUMBER_BID'),
            'COMMENT' => $request->get('COMMENT'),
            'storageTypeId' => $request->get('storageTypeId'),
           // 'PHONE' => $request->get('PHONE'),
           // 'EMAIL' => $request->get('EMAIL'),
           // 'ASSIGNED_BY_ID' => $request->get('ASSIGNED_BY_ID'),
        );

        return $submittedStore;
    }

    private static function validate($store)
    {
        $errors = new ErrorCollection();

        if (empty($store['NUMBER_BID'])) {
            $errors->setError(new Error(Loc::getMessage('CRMBIDS_ERROR_EMPTY_NAME')));
        }

//        if (empty($store['ASSIGNED_BY_ID'])) {
//            $errors->setError(new Error(Loc::getMessage('CRMSTORES_ERROR_EMPTY_ASSIGNED_BY_ID')));
//        } else {
//            $dbUser = UserTable::getById($store['ASSIGNED_BY_ID']);
//            if ($dbUser->getSelectedRowsCount() <= 0) {
//                $errors->setError(new Error(Loc::getMessage('CRMSTORES_ERROR_UNKNOWN_ASSIGNED_BY_ID')));
//            }
//        }

        return $errors;
    }

    private static function isFormSubmitted()
    {
        $context = Context::getCurrent();
        $request = $context->getRequest();
        $saveAndView = $request->get('saveAndView');
        $saveAndAdd = $request->get('saveAndAdd');
        $apply = $request->get('apply');
        return !empty($saveAndView) || !empty($saveAndAdd) || !empty($apply);
    }

    private function getRedirectUrl($savedStoreId = null)
    {
        $context = Context::getCurrent();
        $request = $context->getRequest();

        if (!empty($savedStoreId) && $request->offsetExists('apply')) {
            return CComponentEngine::makePathFromTemplate(
                $this->arParams['URL_TEMPLATES']['EDIT'],
                array('STORE_ID' => $savedStoreId)
            );
        } elseif (!empty($savedStoreId) && $request->offsetExists('saveAndAdd')) {
            return CComponentEngine::makePathFromTemplate(
                $this->arParams['URL_TEMPLATES']['EDIT'],
                array('STORE_ID' => 0)
            );
        }

        $backUrl = $request->get('backurl');
        if (!empty($backUrl)) {
            return $backUrl;
        }

        if (!empty($savedStoreId) && $request->offsetExists('saveAndView')) {
            return CComponentEngine::makePathFromTemplate(
                $this->arParams['URL_TEMPLATES']['DETAIL'],
                array('BID_ID' => $savedStoreId)
            );
        } else {
            return $this->arParams['SEF_FOLDER'];
        }
    }
}