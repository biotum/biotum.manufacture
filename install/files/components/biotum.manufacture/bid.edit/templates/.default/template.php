<?php
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

global $APPLICATION;

use \Bitrix\Crm\Conversion\EntityConverter;
use \Bitrix\Crm\Category\DealCategory;
use Bitrix\Main\Localization\Loc;
use Bitrix\Main\Loader;
/** @var CBitrixComponentTemplate $this */

if (!Loader::includeModule('crm')) {
    ShowError(Loc::getMessage('CRMBIDS_NO_CRM_MODULE'));
    return;
}
$APPLICATION->SetAdditionalCSS('/bitrix/js/crm/css/crm.css');
$APPLICATION->SetAdditionalCSS("/bitrix/themes/.default/crm-entity-show.css");

if(SITE_TEMPLATE_ID === 'bitrix24')
{
    $APPLICATION->SetAdditionalCSS("/bitrix/themes/.default/bitrix24/crm-entity-show.css");
}

if(isset($arResult['CONVERSION_LEGEND'])):
    ?><div class="crm-view-message"><?=$arResult['CONVERSION_LEGEND']?></div><?
endif;

//$jsCoreInit = array('date', 'popup', 'ajax');
//if($arResult['ENABLE_DISK'])
//{
//    $jsCoreInit[] = 'uploader';
//    $jsCoreInit[] = 'file_dialog';
//}
//CJSCore::Init($jsCoreInit);
//
//if($arResult['ENABLE_DISK'])
//{
//    CCrmComponentHelper::RegisterScriptLink('/bitrix/js/crm/disk_uploader.js');
//    $APPLICATION->SetAdditionalCSS('/bitrix/js/disk/css/legacy_uf_common.css');
//}
//if($arResult['ENABLE_WEBDAV'])
//{
//    $APPLICATION->SetAdditionalCSS('/bitrix/components/bitrix/webdav/templates/.default/style.css');
//    $APPLICATION->SetAdditionalCSS('/bitrix/components/bitrix/webdav.user.field/templates/.default/style.css');
//    $APPLICATION->SetAdditionalCSS('/bitrix/js/webdav/css/file_dialog.css');
//
//    CCrmComponentHelper::RegisterScriptLink('/bitrix/js/main/core/core_dd.js');
//    CCrmComponentHelper::RegisterScriptLink('/bitrix/js/main/file_upload_agent.js');
//    CCrmComponentHelper::RegisterScriptLink('/bitrix/js/webdav/file_dialog.js');
//    CCrmComponentHelper::RegisterScriptLink('/bitrix/js/crm/webdav_uploader.js');
//}



$elementID = isset($arResult['BIDS']['NUMBER_BID']) ? $arResult['BIDS']['NUMBER_BID'] : 0;
$arResult['CRM_CUSTOM_PAGE_TITLE'] = GetMessage(
    ($elementID > 0) ? 'CRM_BID_SHOW_TITLE' : 'CRM_BID_SHOW_TITLE',
    array(
        '#BID_NUMBER#' => !empty($arResult['BIDS']['NUMBER_BID']) ? $arResult['BIDS']['NUMBER_BID'] : '-',
        '#BEGINDATE#' => !empty($arResult['BIDS']['DATE_BID']) ? CCrmComponentHelper::TrimDateTimeString(ConvertTimeStamp(MakeTimeStamp($arResult['BIDS']['DATE_BID']), 'SHORT', SITE_ID)) : '-'
    )
);


$arFormButtons['custom_html'] .= $arResult['FORM_CUSTOM_HTML'];

$arTabs = array();
$productFieldset = array();
$arFormButtons['custom_html'] .= $arResult['FORM_CUSTOM_HTML'];
$status=json_decode(COption::GetOptionString('biotum.manufacture', 'APPEAL_FIELD_DESTINATION'),true);
$arTabs=array(
    array(
        'id' => 'tab_1',
        'name' => GetMessage('CRMBIDS_TAB_STORE_NAME'),
        'title' => GetMessage('CRMBIDS_TAB_STORE_TITLE'),
        'display' =>false,
        'fields' => array(
            array(
                'id' => 'section_bid',
                'name' => GetMessage('CRMBIDS_FIELD_SECTION_INFOBID'),
                'type' => 'section',
                'isTactile' => true,
        ),
            array(
                'id' => 'NUMBER_BID',
                'name' => Loc::getMessage('CRMBIDS_FIELD_NAME'),
                'type' => 'label',
                'value' => $arResult['BIDS']['NUMBER_BID'],
                'isTactile' => true,
            ),
            array(
                'id' => 'DATE_BID',
                'name' => Loc::getMessage('CRMBIDS_FIELD_DATE'),
                'type' => 'label',
                'value' => $arResult['BIDS']['DATE_BID'],
                'isTactile' => true,
            ),
            array(
                'id' => 'STATUS_BID',
                'name' => Loc::getMessage('CRMBIDS_FIELD_STATUS_BID'),
                'type' => 'label',
                'value' => $status[$arResult['BIDS']['STATUS_BID']],
                'isTactile' => true,
            ),
            array(
                'id' => 'PRODUCT_ID',
                'name' => Loc::getMessage('CRMBIDS_FIELD_PRODUCT_ID'),
                'type' => 'label',
                'value' =>$arResult['BIDS']['PRODUCT_NAME'],
                'isTactile' => true,
            ),
            array(
                'id' => 'DATETIME_START_MANUFACTURE',
                'name' => Loc::getMessage('CRMBIDS_FIELD_DATE_START_MANUFACTURE'),
                'type' => 'label',
                'value' => 'Дата->'.ConvertDateTime($arResult['BIDS']['DATETIME_START_MANUFACTURE'],'DD-MM-YYYY','ru').'  Время ->'.ConvertDateTime($arResult['BIDS']['DATETIME_START_MANUFACTURE'], "HH:MI", "ru"),
                'isTactile' => true,
            ),
            array(
                'id' => 'DATETIME_SHIPMENTREADY',
                'name' => Loc::getMessage('CRMBIDS_FIELD_DATE_SHIPMENTREADY'),
                'type' => 'label',
                'value' =>'Дата->'.ConvertDateTime($arResult['BIDS']['DATETIME_SHIPMENTREADY'],'DD-MM-YYYY','ru').' Время->'.ConvertDateTime($arResult['BIDS']['DATETIME_SHIPMENTREADY'], "HH:MI", "ru"),
                'isTactile' => true,
            ),
            array(
                'id' => 'WEIGHT',
                'name' => Loc::getMessage('CRMBIDS_FIELD_WEIGHT'),
                'type' => 'label',
                'value' => $arResult['BIDS']['WEIGHT'].' тонн',
                'isTactile' => true,
            ),
            array(
                'id' => 'BUYER_ID',
                'name' => Loc::getMessage('CRMBIDS_FIELD_BUYER_ID'),
                'type' => 'label',
                'value' => $arResult['BIDS']['COMPANY_NAME'],
                'isTactile' => true,
            ),
            array(
                'id' => 'CONSIGNEE_ID',
                'name' => Loc::getMessage('CRMBIDS_FIELD_CONSIGNEE_ID'),
                'type' => 'label',
                'value' => $arResult['BIDS']['CONSIGNEE_NAME'],
                'isTactile' => true,
            ),
            array(
                'id' => 'CONSIGNEE_PHONE',
                'name' =>'Телефон,Email грузополучателя',
                'type' => 'custom',
                'value' =>$arResult['BIDS']['COMPANY_PHONE'].' E-mail <a class="crm-client-contacts-block-text-mail" href="'.$arResult['BIDS']['COMPANY_EMAIL'].'" title="'.$arResult['BIDS']['COMPANY_EMAIL'].'">'.$arResult['BIDS']['COMPANY_EMAIL'].'</a>',
                'isTactile' => true,
            ),
            array(
                'id' => 'CONSIGNEE_CONTACT',
                'name' => GetMessage('CRM_FIELD_COMPANY_CONTACTS'),
                'type' => 'crm_multiple_client_selector',
                'componentParams' => array(
                    'OWNER_TYPE_NAME' => CCrmOwnerType::CompanyName,
                    'OWNER_ID' => $arResult['ELEMENT_ID'],
                    'CONTEXT' => "COMPANY_{$arResult['ELEMENT_ID']}",
                    'READ_ONLY' => true,
                    'ENTITY_TYPE' => CCrmOwnerType::ContactName,
                    'ENTITY_IDS' => \Bitrix\Crm\Binding\ContactCompanyTable::getCompanyContactIDs($arResult['BIDS']['CONSIGNEE_ID']),
                    'ENTITIES_INPUT_NAME' => 'CONTACT_ID',
                    'ENABLE_REQUISITES'=> false,
                    'ENABLE_LAZY_LOAD'=> true,
                    'LOADER' => array(
                        'URL' => '/bitrix/components/bitrix/crm.contact.show/ajax.php?siteID='.SITE_ID.'&'.bitrix_sessid_get()
                    ),
                    'NAME_TEMPLATE' => \Bitrix\Crm\Format\PersonNameFormatter::getFormat()
                ),
                'isTactile' => true
            ),
            array(
                'id' => 'COMMENT',
                'name' => GetMessage('CRMBIDS_FIELD_COMMENT'),
                'type' => 'textarea',
                'value' => $arResult['BIDS']['COMMENT'],
                'isTactile' => false,
            ),
            array(
                'id' => 'INFO',
                'name' => '*',
                'type' => 'label',
                'value' => GetMessage('CRMBIDS_FIELD_INFO'),
                'isTactile' => false,
            ),
            array(
            'id' => 'FILES',
            'name' => 'Файл',
            'params' => array(),
            'type' => 'vertical_container',
            'value' => $arResult['STORAGE']
        ),

            array(
                'id' => 'ASSIGNED_BY',
                'name' => Loc::getMessage('CRMBIDS_FIELD_ASSIGNED_BY'),
                'type' => 'custom',
                'value' => CCrmViewHelper::PrepareFormResponsible(
                    $arResult['BIDS']['ASSIGNED_BY_ID'],
                    CSite::GetNameFormat(),
                    \Bitrix\Main\Config\Option::get('intranet', 'path_user', '', SITE_ID) . '/'
                ),
                'isTactile' => true,
            )
        ),
    ),
    array(
        'id' => 'deals',
        'name' => Loc::getMessage('CRMBIDS_TAB_DEALS_NAME'),
        'title' =>Loc::getMessage('CRMBIDS_TAB_DEALS_TITLE'),
        'fields' => array(
            array(
                'id' => 'DEALS',
                'colspan' => true,
                'type' => 'custom',
                'value' => $boundDealsHtml
            )
        )
    )
);

//ob_start();

$APPLICATION->IncludeComponent(
    'bitrix:crm.interface.form',
    'edit',
    array(
        'GRID_ID' => $arResult['GRID_ID'],
        'FORM_ID' => $arResult['FORM_ID'],
        'ENABLE_TACTILE_INTERFACE' => 'Y',
        'SHOW_SETTINGS' => 'Y',
        'IS_NEW' => $elementID <= 0,
        'TITLE' => $arResult['CRM_CUSTOM_PAGE_TITLE'],
        'DATA' => $arResult['BIDS'],
        'TABS' =>$arTabs,
        'BUTTONS' => array(
            'back_url' => $arResult['BACK_URL'],
            'standard_buttons' => true,
        ),
        // 'FIELD_SETS' => array($productFieldset),
        'USER_FIELD_ENTITY_ID' => CCrmQuote::$sUFEntityID,
        'USER_FIELD_SERVICE_URL' => '/bitrix/components/bitrix/crm.config.fields.edit/ajax.php?siteID=' . SITE_ID . '&' . bitrix_sessid_get(),

    ),
    $this->getComponent(),
    array('HIDE_ICONS' => 'Y')
);

$APPLICATION->IncludeComponent(
    'biotum.manufacture:crm.quote.edit',
    '',
    array(
       'ELEMENT_ID' =>$arResult['BIDS']['ID'],
       // 'ELEMENT'=>$arResult['ELEMENT'],
       // 'NAME_TEMPLATE' => $arParams['NAME_TEMPLATE'],
    ),
    $this->getComponent(),
    array('HIDE_ICONS' => 'Y')
);

$APPLICATION->IncludeComponent(
    'biotum.manufacture:bid.bounddeals',
    '',
    array(
        'DEAL_ID' => $arResult['BIDS']['DEAL_ID'],
        'DELIVERY_ID'=>$arResult['BIDS']['DELIVERY_ID']
    ),
    $this->getComponent(),
    array('HIDE_ICONS' => 'Y')
);

