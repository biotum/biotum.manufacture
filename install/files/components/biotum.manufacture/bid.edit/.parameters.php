<?php
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true)die();
use Bitrix\Main\Localization\Loc;
if(!CModule::IncludeModule('crm'))
	return false;

$arComponentParameters = Array(
	'PARAMETERS' => array(	
		'ELEMENT_ID' => array(
			'PARENT' => 'BASE',
			'NAME' => GetMessage('CRM_ELEMENT_ID'),
			'TYPE' => 'STRING',
			'DEFAULT' => '={$_REQUEST["bid_id"]}'
		),
        'SEF_MODE' => array(
            'details' => array(
                'NAME' => Loc::getMessage('CRMBIDS_DETAILS_URL_TEMPLATE'),
                'DEFAULT' => '#BID_ID#/',
                'VARIABLES' => array('BID_ID')
            ),
            'edit' => array(
                'NAME' => Loc::getMessage('CRMBIDS_EDIT_URL_TEMPLATE'),
                'DEFAULT' => '#BID_ID#/edit/',
                'VARIABLES' => array('BID_ID')
            ),

        )
	)	
);
?>