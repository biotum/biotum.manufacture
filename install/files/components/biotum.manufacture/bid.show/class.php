<?php
defined('B_PROLOG_INCLUDED') || die;

use Biotum\Manufacture\Entity\BidsTable;
use Biotum\Manufacture\BizProc\BidDocument;
use Bitrix\Crm\CompanyTable;
use Bitrix\Main\Loader;
use Bitrix\Main\Localization\Loc;
use Bitrix\Main\Context;
use Bitrix\Main\Web\Uri;
use Bitrix\Rest\PlacementTable;

class CBiotumManufactureBidShowComponent extends CBitrixComponent
{
    const FORM_ID = 'CRMBID_SHOW';
    const BIZPROC_TAB_ID = 'bizproc';

    public function __construct(CBitrixComponent $component = null)
    {
        parent::__construct($component);

        CBitrixComponent::includeComponentClass('biotum.manufacture:bid.list');
        CBitrixComponent::includeComponentClass('biotum.manufacture:bid.edit');

    }

    public function executeComponent()
    {
        global $APPLICATION;

        $APPLICATION->SetTitle(Loc::getMessage('CRMBIDS_SHOW_TITLE_DEFAULT'));

        if (!Loader::includeModule('biotum.manufacture')) {
            ShowError(Loc::getMessage('CRMBIDS_NO_MODULE'));
            return;
        }

        $dbStore = BidsTable::getById($this->arParams['BID_ID']);
        $bids = $dbStore->fetch();

        if (empty($bids)) {
            ShowError(Loc::getMessage('CRMBIDS_STORE_NOT_FOUND'));
            return;
        }

        $APPLICATION->SetTitle(Loc::getMessage(
            'CRMBID_SHOW_TITLE',
            array(
                '#ID#' => $bids['ID'],
               '#NUMBER_BID#' => $bids['NUMBER_BID'],
               '#STATUS_BID#' => $bids['STATUS_BID'],
//                '#PHONE#' => $store['PHONE'],
//                '#EMAIL#' => $store['EMAIL'],
            )
        ));

        $this->arResult =array(
            'FORM_ID' => self::FORM_ID,
            'TACTILE_FORM_ID' => CBiotumManufactureBidsEditComponent::FORM_ID,
            'GRID_ID' => CBiotumManufactureBidsListComponent::GRID_ID,
            'BIDS' => \Biotum\Manufacture\Entity\ManufacturreBids::getlistID(['filter'=>['ID'=>$this->arParams['BID_ID']]]),
            'BIZPROC_TAB' => array(
                'ID' => self::BIZPROC_TAB_ID,
                'HTML' => $this->getBizprocTabHtml($bids['ID']),
            ),
            'PLACEMENTS' => $this->getPlacements(),
        );

        $this->includeComponentTemplate();
    }

    private function getProduct($params)
    {
        $product = CCrmProduct::GetByID($params['PRODUCT_ID']);
        return ['PRODUCT_NAME'=>$product['NAME']];
    }

    private function getBizprocTabHtml($storeId)
    {
        if (!Loader::includeModule('bizproc')) {
            return null;
        }

        $context = Context::getCurrent();
        $request = $context->getRequest();

        $workflowIdLog = $request->get('bizproc_log');
        $workflowIdTask = $request->get('bizproc_task');
        $workflowStart = $request->get('bizproc_start');

        if (!empty($workflowIdLog)) {
            return $this->getWorkflowLogHtml($storeId, $workflowIdLog);
        } elseif (!empty($workflowIdTask)) {
            return $this->getWorkflowTaskHtml($workflowIdTask);
        } elseif ($workflowStart == 'Y') {
            return $this->getWorkflowStartHtml($storeId);
        } else {
            return $this->getWorkflowListHtml($storeId);
        }
    }

    private function getWorkflowListHtml($storeId)
    {
        global $APPLICATION;

        $context = Context::getCurrent();
        $request = $context->getRequest();

        $requestUri = new Uri($request->getRequestUri());
        $requestUri->addParams(array(
            self::FORM_ID . '_active_tab' => self::BIZPROC_TAB_ID,
        ));
        $requestUri->deleteParams(array('action'));

        $logUrlTemplate = CHTTP::urlAddParams($requestUri->getUri(), array('bizproc_log' => '#ID#'));
        $taskUrlTemplate = CHTTP::urlAddParams($requestUri->getUri(), array('bizproc_task' => '#ID#'));
        $startUrl = CHTTP::urlAddParams($requestUri->getUri(), array('bizproc_start' => 'Y'));

        ob_start();
        $APPLICATION->IncludeComponent(
            'bitrix:bizproc.document',
            '',
            array(
                'MODULE_ID' => 'biotum.manufacture',
                'ENTITY' => BidDocument::class,
                'DOCUMENT_TYPE' => 'bid',
                'DOCUMENT_ID' => $storeId,
                'TASK_EDIT_URL' => $taskUrlTemplate,
                'WORKFLOW_LOG_URL' => $logUrlTemplate,
                'WORKFLOW_START_URL' => $startUrl,
                'POST_FORM_URI' => '',
                'back_url' => $requestUri->getUri(),
                'SET_TITLE' => 'N'
            ),
            null,
            array('HIDE_ICONS' => 'Y')
        );
        return ob_get_clean();
    }

    private function getWorkflowLogHtml($storeId, $workflowId)
    {
        global $APPLICATION;

        ob_start();
        $APPLICATION->IncludeComponent(
            'bitrix:bizproc.log',
            '',
            array(
                'MODULE_ID' => 'biotum.manufacture',
                'ENTITY' => BidDocument::class,
                'DOCUMENT_TYPE' => 'bid',
                'COMPONENT_VERSION' => 2,
                'DOCUMENT_ID' => $storeId,
                'ID' => $workflowId,
                'SET_TITLE' => 'N',
                'INLINE_MODE' => 'Y',
                'AJAX_MODE' => 'N',
                'NAME_TEMPLATE' => CSite::GetNameFormat()
            ),
            null,
            array('HIDE_ICONS' => 'Y')
        );
        return ob_get_clean();
    }

    private function getWorkflowTaskHtml($taskId)
    {
        global $APPLICATION;

        $context = Context::getCurrent();
        $request = $context->getRequest();

        ob_start();
        $APPLICATION->IncludeComponent(
            'bitrix:bizproc.task',
            '',
            Array(
                'TASK_ID' => $taskId,
                'USER_ID' => 0,
                'WORKFLOW_ID' => '',
                'DOCUMENT_URL' => $request->getRequestUri(),
                'SET_TITLE' => 'N',
                'SET_NAV_CHAIN' => 'N'
            ),
            null,
            array('HIDE_ICONS' => 'Y')
        );
        return ob_get_clean();
    }

    private function getWorkflowStartHtml($storeId)
    {
        global $APPLICATION;

        $context = Context::getCurrent();
        $request = $context->getRequest();

        ob_start();
        $APPLICATION->IncludeComponent(
            'bitrix:bizproc.workflow.start',
            '',
            array(
                'MODULE_ID' => 'biotum.manufacture',
                'ENTITY' => BidDocument::class,
                'DOCUMENT_TYPE' => 'bid',
                'DOCUMENT_ID' => $storeId,
                'TEMPLATE_ID' => $request->get('workflow_template_id'),
                'SET_TITLE'	=>	'N'
            ),
            null,
            array('HIDE_ICONS' => 'Y')
        );
        return ob_get_clean();
    }

    private function getPlacements()
    {
        return PlacementTable::getHandlersList('CRMBID_BID_DETAILS');
    }
}