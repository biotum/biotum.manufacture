<?php
defined('B_PROLOG_INCLUDED') || die;

use Bitrix\Main\Config\Option;
use Bitrix\Main\Loader;
use Bitrix\Main\Localization\Loc;


/** @var CBitrixComponentTemplate $this */

if (!Loader::includeModule('crm')) {
    ShowError(Loc::getMessage('CRMBIDS_NO_CRM_MODULE'));
    return;
}
$arResult['CRM_CUSTOM_PAGE_TITLE'] = GetMessage(
    ($elementID > 0) ? 'CRM_BID_SHOW_TITLE' : 'CRM_BID_SHOW_TITLE',
    array(
        '#BID_NUMBER#' => !empty($arResult['BIDS']['NUMBER_BID']) ? $arResult['BIDS']['NUMBER_BID'] : '-',
        '#BEGINDATE#' => !empty($arResult['BIDS']['DATE_BID']) ? CCrmComponentHelper::TrimDateTimeString(ConvertTimeStamp(MakeTimeStamp($arResult['BIDS']['DATE_BID']), 'SHORT', SITE_ID)) : '-'
    )
);
ob_start();
$APPLICATION->IncludeComponent(
    'biotum.manufacture:bid.bounddeals',
    '',
    array(
        'DEAL_ID' => $arResult['BIDS']['DEAL_ID'],
        'DELIVERY_ID'=>$arResult['BIDS']['DELIVERY_ID']
    ),
    $this->getComponent(),
    array('HIDE_ICONS' => 'Y')
);
$boundDealsHtml = ob_get_clean();

$status=json_decode(COption::GetOptionString('biotum.manufacture', 'APPEAL_FIELD_DESTINATION'),true);


$tabs =array(
    array(
        'id' => 'tab_1',
        'name' => Loc::getMessage('CRMBIDS_TAB_STORE_NAME'),
        'title' => Loc::getMessage('CRMBIDS_TAB_STORE_TITLE'),
        'display' => false,
        'fields' => array(
            array(
                'id' => 'section_store',
                'name' => Loc::getMessage('CRMBIDS_FIELD_SECTION_BID'),
                'type' => 'section',
                'isTactile' => true,
            ),
//                    array(
//                        'id' => 'ID',
//                        'name' => Loc::getMessage('CRMBIDS_FIELD_ID'),
//                        'type' => 'label',
//                        'value' => $arResult['BID']['ID'],
//                        'isTactile' => true,
//                    ),
            array(
                'id' => 'WORKFLOW_VIEW',
               // 'colspan' => true,
                'type' => 'custom',
                'name' => Loc::getMessage('CRMBIDS_FIELD_BIZPROC'),
                'value' => $arResult['BIZPROC_TAB']['HTML'],
                 'isTactile' => true,
            ),
            array(
                'id' => 'NUMBER_BID',
                'name' => Loc::getMessage('CRMBIDS_FIELD_NAME'),
                'type' => 'label',
                'value' => $arResult['BIDS']['NUMBER_BID'],
                'isTactile' => true,
            ),
            array(
                'id' => 'DATE_BID',
                'name' => Loc::getMessage('CRMBIDS_FIELD_DATE'),
                'type' => 'label',
                'value' => $arResult['BIDS']['DATE_BID'],
                'isTactile' => true,
            ),
            array(
                'id' => 'STATUS_BID',
                'name' => Loc::getMessage('CRMBIDS_FIELD_STATUS_BID'),
                'type' => 'label',
                'value' => $status[$arResult['BIDS']['STATUS_BID']],
                'isTactile' => true,
            ),
            array(
                'id' => 'PRODUCT_ID',
                'name' => Loc::getMessage('CRMBIDS_FIELD_PRODUCT_ID'),
                'type' => 'label',
                'value' =>$arResult['BIDS']['PRODUCT_NAME'],
                'isTactile' => true,
            ),
            array(
                'id' => 'DATETIME_START_MANUFACTURE',
                'name' => Loc::getMessage('CRMBIDS_FIELD_DATE_START_MANUFACTURE'),
                'type' => 'label',
                'value' => 'Дата->'.ConvertDateTime($arResult['BIDS']['DATETIME_START_MANUFACTURE'],'DD-MM-YYYY','ru').'  Время ->'.ConvertDateTime($arResult['BIDS']['DATETIME_START_MANUFACTURE'], "HH:MI", "ru"),
                'isTactile' => true,
            ),
            array(
                'id' => 'DATETIME_SHIPMENTREADY',
                'name' => Loc::getMessage('CRMBIDS_FIELD_DATE_SHIPMENTREADY'),
                'type' => 'label',
                'value' =>'Дата->'.ConvertDateTime($arResult['BIDS']['DATETIME_SHIPMENTREADY'],'DD-MM-YYYY','ru').' Время->'.ConvertDateTime($arResult['BIDS']['DATETIME_SHIPMENTREADY'], "HH:MI", "ru"),
                'isTactile' => true,
            ),
            array(
                'id' => 'WEIGHT',
                'name' => Loc::getMessage('CRMBIDS_FIELD_WEIGHT'),
                'type' => 'label',
                'value' => $arResult['BIDS']['WEIGHT'].' тонн',
                'isTactile' => true,
            ),
            array(
                'id' => 'BUYER_ID',
                'name' => Loc::getMessage('CRMBIDS_FIELD_BUYER_ID'),
                'type' => 'label',
                'value' => $arResult['BIDS']['COMPANY_NAME'],
                'isTactile' => true,
            ),
            array(
                'id' => 'CONSIGNEE_ID',
                'name' => Loc::getMessage('CRMBIDS_FIELD_CONSIGNEE_ID'),
                'type' => 'label',
                'value' => $arResult['BIDS']['CONSIGNEE_NAME'],
                'isTactile' => true,
            ),
            array(
                'id' => 'CONSIGNEE_PHONE',
                'name' =>'Телефон,Email грузополучателя',
                'type' => 'custom',
                'value' =>$arResult['BIDS']['COMPANY_PHONE'].' E-mail <a class="crm-client-contacts-block-text-mail" href="'.$arResult['BIDS']['COMPANY_EMAIL'].'" title="'.$arResult['BIDS']['COMPANY_EMAIL'].'">'.$arResult['BIDS']['COMPANY_EMAIL'].'</a>',
                'isTactile' => true,
            ),
            array(
                'id' => 'CONSIGNEE_CONTACT',
                'name' => GetMessage('CRM_FIELD_COMPANY_CONTACTS'),
                'type' => 'crm_multiple_client_selector',
                'componentParams' => array(
                    'OWNER_TYPE_NAME' => CCrmOwnerType::CompanyName,
                    'OWNER_ID' => $arResult['ELEMENT_ID'],
                    'CONTEXT' => "COMPANY_{$arResult['ELEMENT_ID']}",
                    'READ_ONLY' => true,
                    'ENTITY_TYPE' => CCrmOwnerType::ContactName,
                    'ENTITY_IDS' => \Bitrix\Crm\Binding\ContactCompanyTable::getCompanyContactIDs($arResult['BIDS']['CONSIGNEE_ID']),
                    'ENTITIES_INPUT_NAME' => 'CONTACT_ID',
                    'ENABLE_REQUISITES'=> false,
                    'ENABLE_LAZY_LOAD'=> true,
                    'LOADER' => array(
                        'URL' => '/bitrix/components/bitrix/crm.contact.show/ajax.php?siteID='.SITE_ID.'&'.bitrix_sessid_get()
                    ),
                    'NAME_TEMPLATE' => \Bitrix\Crm\Format\PersonNameFormatter::getFormat()
                ),
                'isTactile' => true
            ),
            array(
                'id' => 'COMMENT',
                'name' => Loc::getMessage('CRMBIDS_FIELD_COMMENT'),
                'type' => 'label',
                'value' => $arResult['BIDS']['COMMENT'],
                'isTactile' => true,
            ),
            array(
                'id' => 'ASSIGNED_BY',
                'name' => Loc::getMessage('CRMBIDS_FIELD_ASSIGNED_BY'),
                'type' => 'custom',
                'value' => CCrmViewHelper::PrepareFormResponsible(
                    $arResult['BIDS']['ASSIGNED_BY_ID'],
                    CSite::GetNameFormat(),
                    Option::get('intranet', 'path_user', '', SITE_ID) . '/'
                ),
                'isTactile' => true,
            ),

        )

    ),

    array(
        'id' => 'deals',
        'name' => Loc::getMessage('CRMBIDS_TAB_DEALS_NAME'),
        'title' =>Loc::getMessage('CRMBIDS_TAB_DEALS_TITLE'),
        'fields' => array(
            array(
                'id' => 'DEALS',
                'colspan' => true,
                'type' => 'custom',
                'value' => $boundDealsHtml
            )
        )
    ),



);

if (!empty($arResult['BIZPROC_TAB'])) {
//    $tabs[] = array(
//        'id' => $arResult['BIZPROC_TAB']['ID'],
//        'name' => Loc::getMessage('CRMSTORES_TAB_BP_NAME'),
//        'title' => Loc::getMessage('CRMSTORES_TAB_BP_TITLE'),
//        'fields' => array(
//            array(
//                'id' => 'WORKFLOW_VIEW',
//                'colspan' => true,
//                'type' => 'custom',
//                'value' => $arResult['BIZPROC_TAB']['HTML']
//            )
//        )
//    );
}

$APPLICATION->IncludeComponent(
    'bitrix:crm.interface.form',
    'show',
    array(
        'GRID_ID' => $arResult['GRID_ID'],
        'FORM_ID' => $arResult['FORM_ID'],
        'TACTILE_FORM_ID' => $arResult['TACTILE_FORM_ID'],
        'ENABLE_TACTILE_INTERFACE' => 'Y',
        'SHOW_SETTINGS' => 'Y',
        'TITLE' => $arResult['CRM_CUSTOM_PAGE_TITLE'],
        'DATA' => $arResult['BIDS'],
        'TABS' =>$tabs,
    ),
    $this->getComponent(),
    array('HIDE_ICONS' => 'Y')
);

foreach ($arResult['PLACEMENT'] as $placementHandler) {

    $tabId = 'tab_rest_' . $placementHandler['ID'];
    $wrapperId = 'placement_' . $placementHandler['ID'] . '_wrapper';
    $serviceUrl = '/bitrix/components/bitrix/app.layout/lazyload.ajax.php?&site=' . SITE_ID . '&' . bitrix_sessid_get();
    $loaderId = strtolower($arParams['FORM_ID'] . '_' . $tabId);
    $componentData = array(
        'template' => '',
        'params' => array(
            'PLACEMENT' => 'CRMBID_BID_DETAILS',
            'PLACEMENT_OPTIONS' => array(
                'ID' => $arResult['BIDS']['ID'],
            ),
            'ID' => $placementHandler['APP_ID'],
            'PLACEMENT_ID' => $placementHandler['ID'],
        ),
    );

    $tabs[] = array(
        'id' => $tabId,
        'name' => strlen($placementHandler['TITLE']) > 0
            ? $placementHandler['TITLE']
            : $placementHandler['APP_NAME'],
        'fields' => array(
            array(
                'id' => $tabId,
                'colspan' => true,
                'type' => 'custom',
                'value' => '<div id="' . $wrapperId . '"></div>'
            )
        ),
    );

    ?>
    <script>
        BX.ready(function () {
            BX.CrmFormTabLazyLoader.create(
                <?= Json::encode($loaderId) ?>,
                <?= Json::encode(array(
                    'containerID' => $wrapperId,
                    'serviceUrl' => $serviceUrl,
                    'formID' => $arResult['FORM_ID'],
                    'tabID' => $tabId,
                    'params' => $componentData,
                )) ?>
            );
        });
    </script>
    <?
}


