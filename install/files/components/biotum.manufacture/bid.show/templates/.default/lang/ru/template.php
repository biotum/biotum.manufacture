<?php
defined('B_PROLOG_INCLUDED') || die;

$MESS['CRMSTORES_NO_CRM_MODULE'] = 'Модуль CRM не установлен.';
$MESS['CRMBIDS_TAB_STORE_NAME'] = 'Заявка на производство';
$MESS['CRMBIDS_TAB_STORE_TITLE'] = 'Свойства Пункта разгрузки';
$MESS['CRMBIDS_FIELD_SECTION_BID'] = 'Заявка на производство';
$MESS['CRMBIDS_FIELD_SECTION_INFOBID']='Информация по заявке';
$MESS["CRM_BID_SHOW_TITLE"] = "Заявка № #BID_NUMBER# от #BEGINDATE#";
$MESS['CRMBIDS_FIELD_ID'] = 'ID';
$MESS['CRMBIDS_FIELD_NAME'] = 'Номер заявки';
$MESS['CRMBIDS_FIELD_DATE'] = 'Дата создания заявки';
$MESS['CRMBIDS_FIELD_DATE_START_MANUFACTURE'] = 'Начало производства';
$MESS['CRMBIDS_FIELD_TIME_START_MANUFACTURE'] = 'Время начала пр-а';
$MESS['CRMBIDS_FIELD_DATE_SHIPMENTREADY'] = 'Готовность к отгрузке';
$MESS['CRMBIDS_FIELD_DATE_TIME_SHIPMENTREADY'] = 'Время г-сти к отгрузке';
$MESS['CRMBIDS_FIELD_PRODUCT_ID'] = 'Продукт';
$MESS['CRMBIDS_FIELD_BUYER_ID'] = 'Покупатель';
$MESS['CRMBIDS_FIELD_CONSIGNEE_ID']='Грузополучатель';
$MESS['CRMBIDS_FIELD_WEIGHT']='Общий вес';
$MESS['CRMBIDS_FIELD_STATUS_BID']='Статус заявки';
$MESS['CRMBIDS_FIELD_DEAL_ID']='Базис поставки';
$MESS['CRMBIDS_FIELD_COMMENT']='Примечание';
$MESS['CRMBIDS_FIELD_ASSIGNED_BY']='Ответственный';
$MESS['CRMBIDS_FIELD_BIZPROC']='Задачи для согласований';
$MESS['CRM_FIELD_COMPANY_CONTACTS']='Контакты грузополучателя';
$MESS['CRMSTORES_TAB_BP_NAME'] = 'Бизнес-процессы';
$MESS['CRMSTORES_TAB_BP_TITLE'] = 'Управление бизнес-процессами заявок на производство';
$MESS['CRMBIDS_TAB_DEALS_NAME'] = 'Базис поставки';
$MESS['CRMBIDS_TAB_COMPANY_NAME'] = 'Клиенты';
$MESS['CRMBIDS_TAB_DEALS_TITLE'] = 'Связанные сделки';
