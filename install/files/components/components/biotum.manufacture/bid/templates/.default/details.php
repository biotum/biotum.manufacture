<?php
defined('B_PROLOG_INCLUDED') || die;

use Bitrix\Main\Localization\Loc;
use Bitrix\Main\Web\Uri;


/** @var CBitrixComponentTemplate $this */


$APPLICATION->IncludeComponent(
    'bitrix:crm.control_panel',
    '',
    array(
        'ID' => 'BID',
        'ACTIVE_ITEM_ID' => 'BID',
    ),
    $component
);

$urlTemplates = array(
    'DETAIL' =>$arResult['SEF_FOLDER'] . $arResult['SEF_URL_TEMPLATES']['details'],
    'EDIT' => $arResult['SEF_FOLDER'] . $arResult['SEF_URL_TEMPLATES']['edit'],
);

$editUrl = CComponentEngine::makePathFromTemplate(
    $urlTemplates['EDIT'],
    array('BID_ID' => $arResult['VARIABLES']['BID_ID'])
);

//$viewUrl = CComponentEngine::makePathFromTemplate(
//    $urlTemplates['DETAIL'],
//    array('BID_ID' => $arResult['VARIABLES']['BID_ID'])
//);

$editUrl = new Uri($editUrl);
//$editUrl->addParams(array('backurl' => $viewUrl));

$APPLICATION->IncludeComponent(
    'bitrix:crm.interface.toolbar',
    'type2',
    array(
        'TOOLBAR_ID' => 'CRMBIDS_TOOLBAR',
        'BUTTONS' => array(
            array(
                'TEXT' => Loc::getMessage('CRMBID_EDIT'),
                'TITLE' => Loc::getMessage('CRMBID_EDIT'),
                'LINK' => $editUrl->getUri(),
                'ICON' => 'btn-edit',
            ),
        )
    ),
    $this->getComponent(),
    array('HIDE_ICONS' => 'Y')
);


$APPLICATION->IncludeComponent(
    'biotum.manufacture:bid.show',
    '',
    array(
        'BID_ID' => $arResult['VARIABLES']['BID_ID']
    ),
   $this->getComponent(),
    array('HIDE_ICONS' => 'Y',)
);

