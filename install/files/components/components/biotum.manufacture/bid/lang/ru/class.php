<?php
defined('B_PROLOG_INCLUDED') || die;

$MESS['CRMBIDS_SEF_NOT_ENABLED'] = 'Режим ЧПУ должен быть включен.';
$MESS['CRMBIDS_SEF_BASE_EMPTY'] = 'Не указан каталог ЧПУ.';