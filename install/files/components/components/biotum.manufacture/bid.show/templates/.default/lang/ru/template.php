<?php
defined('B_PROLOG_INCLUDED') || die;

$MESS['CRMSTORES_NO_CRM_MODULE'] = 'Модуль CRM не установлен.';
$MESS['CRMBIDS_TAB_STORE_NAME'] = 'Заявка на производство';
$MESS['CRMBIDS_TAB_STORE_TITLE'] = 'Свойства Пункта разгрузки';
$MESS['CRMBIDS_FIELD_SECTION_BID'] = 'Заявка на производство';
$MESS['CRMSTORES_FIELD_ID'] = 'ID';
$MESS['CRMSTORES_FIELD_NAME'] = 'Номер заявки';
$MESS['CRMSTORES_FIELD_STATUS_BID'] = 'Статус заявки';
$MESS['CRMSTORES_FIELD_PRODUCT_ID'] = 'Продукт';
$MESS['CRMSTORES_FIELD_PHONE'] = 'Телефон';
$MESS['CRMSTORES_FIELD_EMAIL'] = 'E-MAIL';
$MESS['CRMSTORES_FIELD_ASSIGNED_BY'] = 'Ответственный';
$MESS['CRMSTORES_TAB_DEALS_NAME'] = 'Сделки';
$MESS['CRMSTORES_TAB_COMPANY_NAME'] = 'Клиенты';
$MESS['CRMSTORES_TAB_DEALS_TITLE'] = 'Связанные сделки';
