<?php
defined('B_PROLOG_INCLUDED') || die;

use Bitrix\Main\Config\Option;
use Bitrix\Main\Loader;
use Bitrix\Main\Localization\Loc;

/** @var CBitrixComponentTemplate $this */

if (!Loader::includeModule('crm')) {
    ShowError(Loc::getMessage('CRMBIDS_NO_CRM_MODULE'));
    return;
}

ob_start();
//$APPLICATION->IncludeComponent(
//    'biotum.manufacture:bid.bounddeals',
//    '',
//    array(
//        'BID_ID' => $arResult['BID']['ID']
//    ),
//    $this->getComponent(),
//    array('HIDE_ICONS' => 'Y')
//);


$boundDealsHtml = ob_get_clean();

$APPLICATION->IncludeComponent(
    'bitrix:crm.interface.form',
    'show',
    array(
        'GRID_ID' => $arResult['GRID_ID'],
        'FORM_ID' => $arResult['FORM_ID'],
        'TACTILE_FORM_ID' => $arResult['TACTILE_FORM_ID'],
        'ENABLE_TACTILE_INTERFACE' => 'Y',
        'SHOW_SETTINGS' => 'Y',
        'DATA' => $arResult['BIDS'],
        'TABS' => array(
            array(
                'id' => 'tab_1',
                'name' => Loc::getMessage('CRMBIDS_TAB_STORE_NAME'),
                'title' => Loc::getMessage('CRMBIDS_TAB_STORE_TITLE'),
                'display' => false,
                'fields' => array(
                    array(
                        'id' => 'section_store',
                        'name' => Loc::getMessage('CRMBIDS_FIELD_SECTION_BID'),
                        'type' => 'section',
                        'isTactile' => true,
                    ),
//                    array(
//                        'id' => 'ID',
//                        'name' => Loc::getMessage('CRMBIDS_FIELD_ID'),
//                        'type' => 'label',
//                        'value' => $arResult['BID']['ID'],
//                        'isTactile' => true,
//                    ),
                    array(
                        'id' => 'NUMBER_BID',
                        'name' => Loc::getMessage('CRMSTORES_FIELD_NAME'),
                        'type' => 'label',
                        'value' => $arResult['BIDS']['NUMBER_BID'],
                        'isTactile' => true,
                    ),
                    array(
                        'id' => 'STATUS_BID',
                        'name' => Loc::getMessage('CRMSTORES_FIELD_STATUS_BID'),
                        'type' => 'label',
                        'value' => $arResult['BIDS']['STATUS_BID'],
                        'isTactile' => true,
                    ),
                    array(
                        'id' => 'PRODUCT_ID',
                        'name' => Loc::getMessage('CRMSTORES_FIELD_PRODUCT_ID'),
                        'type' => 'label',
                        'value' =>$arResult['BIDS']['PRODUCT_NAME'],
                        'isTactile' => true,
                    ),
//                    array(
//                        'id' => 'PHONE',
//                        'name' => Loc::getMessage('CRMSTORES_FIELD_PHONE'),
//                        'type' => 'label',
//                        'value' => $arResult['BID']['PHONE'],
//                        'isTactile' => true,
//                    ),
//                    array(
//                        'id' => 'EMAIL',
//                        'name' => Loc::getMessage('CRMSTORES_FIELD_EMAIL'),
//                        'type' => 'label',
//                        'value' => $arResult['BID']['EMAIL'],
//                        'isTactile' => true,
//                    ),
//                    array(
//                        'id' => 'ASSIGNED_BY',
//                        'name' => Loc::getMessage('CRMSTORES_FIELD_ASSIGNED_BY'),
//                        'type' => 'custom',
//                        'value' => CCrmViewHelper::PrepareFormResponsible(
//                            $arResult['STORE']['ASSIGNED_BY_ID'],
//                            CSite::GetNameFormat(),
//                            Option::get('intranet', 'path_user', '', SITE_ID) . '/'
//                        ),
//                        'isTactile' => true,
//                    )
                )

            ),

//            array(
//                'id' => 'deals',
//                'name' => Loc::getMessage('CRMBIDS_TAB_DEALS_NAME'),
//                'title' =>Loc::getMessage('CRMBIDS_TAB_DEALS_TITLE'),
//                'fields' => array(
//                    array(
//                        'id' => 'DEALS',
//                        'colspan' => true,
//                        'type' => 'custom',
//                        'value' => $boundDealsHtml
//                    )
//                )
//            ),



        ),
    ),
    $this->getComponent(),
    array('HIDE_ICONS' => 'Y')
);
