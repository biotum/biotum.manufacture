<?php
defined('B_PROLOG_INCLUDED') || die;

$MESS['CRMSTORES_NO_MODULE'] = 'Модуль "Заявка на производство" не установлен.';
$MESS['CRMSTORES_STORE_NOT_FOUND'] = 'Заявки на производство не существует.';
$MESS['CRMSTORES_SHOW_TITLE_DEFAULT'] = 'Заявка на производство';
$MESS['CRMSTORES_SHOW_TITLE'] = 'Заявка на производство №#ID# &mdash; #ID#';