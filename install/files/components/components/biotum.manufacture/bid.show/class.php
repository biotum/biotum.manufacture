<?php
defined('B_PROLOG_INCLUDED') || die;

use Biotum\Manufacture\Entity\BidsTable;
use Bitrix\Crm\CompanyTable;
use Bitrix\Main\Loader;
use Bitrix\Main\Localization\Loc;

class CBiotumManufactureBidShowComponent extends CBitrixComponent
{
    const FORM_ID = 'CRMBID_SHOW';

    public function __construct(CBitrixComponent $component = null)
    {
        parent::__construct($component);

        CBitrixComponent::includeComponentClass('biotum.manufacture:bid.list');
        CBitrixComponent::includeComponentClass('biotum.manufacture:bid.edit');

    }

    public function executeComponent()
    {
        global $APPLICATION;

        $APPLICATION->SetTitle(Loc::getMessage('CRMBIDS_SHOW_TITLE_DEFAULT'));

        if (!Loader::includeModule('biotum.manufacture')) {
            ShowError(Loc::getMessage('CRMBIDS_NO_MODULE'));
            return;
        }

        $dbStore = BidsTable::getById($this->arParams['BID_ID']);
        $bids = $dbStore->fetch();
 // print_r(array_replace_recursive($bids,$this->getProduct($bids)));

        if (empty($bids)) {
            ShowError(Loc::getMessage('CRMBIDS_STORE_NOT_FOUND'));
            return;
        }

        $APPLICATION->SetTitle(Loc::getMessage(
            'CRMBID_SHOW_TITLE',
            array(
                '#ID#' => $bids['ID'],
               '#NUMBER_BID#' => $bids['NUMBER_BID'],
               '#STATUS_BID#' => $bids['STATUS_BID'],
//                '#PHONE#' => $store['PHONE'],
//                '#EMAIL#' => $store['EMAIL'],
            )
        ));

        $this->arResult =array(
            'FORM_ID' => self::FORM_ID,
            'TACTILE_FORM_ID' => CBiotumManufactureBidsEditComponent::FORM_ID,
            'GRID_ID' => CBiotumManufactureBidsListComponent::GRID_ID,
            'BIDS' => array_replace_recursive($bids,$this->getProduct($bids))
        );

        $this->includeComponentTemplate();
    }

    private function getProduct($params)
    {
        $product = CCrmProduct::GetByID($params['PRODUCT_ID']);
        return ['PRODUCT_NAME'=>$product['NAME']];
    }
}