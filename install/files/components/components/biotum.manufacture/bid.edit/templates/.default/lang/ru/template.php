<?php
defined('B_PROLOG_INCLUDED') || die;

$MESS['CRMSTORES_NO_CRM_MODULE'] = 'Модуль CRM не установлен.';
$MESS['CRMSTORES_TAB_STORE_NAME'] = 'Заявка на производство';
$MESS['CRMSTORES_TAB_STORE_TITLE'] = 'Свойства Заявки на производство';
$MESS['CRMSTORES_FIELD_SECTION_STORE'] = 'Заявка на производство';
$MESS['CRMSTORES_FIELD_ID'] = 'ID';
$MESS['CRMSTORES_FIELD_NAME'] = 'Номер заявки';
$MESS['CRMSTORES_FIELD_ADDRESS'] = 'Адрес';
$MESS['CRMSTORES_FIELD_FIO'] = 'ФИО';
$MESS['CRMSTORES_FIELD_PHONE'] = 'Телефон';
$MESS['CRMSTORES_FIELD_EMAIL'] = 'E-MAIL';
$MESS['CRMSTORES_FIELD_ASSIGNED_BY'] = 'Ответственный';
$MESS['CRMSTORES_TAB_DEALS_NAME'] = 'Компании';
$MESS['CRMSTORES_TAB_DEALS_TITLE'] = 'Связанные сделки';