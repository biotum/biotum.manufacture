<?php
defined('B_PROLOG_INCLUDED') || die;

use Biotum\Manufacture\Entity\BidsTable;
use Bitrix\Main\Context;
use Bitrix\Main\Entity\Event;
use Bitrix\Main\Entity\EventResult;
use Bitrix\Main\Error;
use Bitrix\Main\ErrorCollection;
use Bitrix\Main\Loader;
use Bitrix\Main\Localization\Loc;
use Bitrix\Main\UserTable;

class CBiotumManufactureBidsEditComponent extends CBitrixComponent
{
    const FORM_ID = 'CRMBIDS_EDIT';

    private $errors;

    public function __construct(CBitrixComponent $component = null)
    {
        parent::__construct($component);

        $this->errors = new ErrorCollection();

        CBitrixComponent::includeComponentClass('biotum.manufacture:bid.list');
    }

    public function executeComponent()
    {
        global $APPLICATION;

        $title = Loc::getMessage('CRMBID_SHOW_TITLE_DEFAULT');

        if (!Loader::includeModule('biotum.manufacture')) {
            ShowError(Loc::getMessage('CRMBID_NO_MODULE'));
            return;
        }

        $store = array(
            'NUMBER_BID' => '',
//            'ADDRESS' => '',
//            'FIO' => '',
//            'PHONE' => '',
//            'EMAIL' => '',
            //'ASSIGNED_BY_ID' => 0
        );

        if (intval($this->arParams['BID_ID']) > 0) {
            $dbStore = BidsTable::getById($this->arParams['BID_ID']);
            $store = $dbStore->fetch();

            if (empty($store)) {
                ShowError(Loc::getMessage('CRMBID_BID_NOT_FOUND'));
                return;
            }
        }

        if (!empty($store['ID'])) {
            $title = Loc::getMessage(
                'CRMBID_SHOW_TITLE',
                array(
                    '#ID#' => $store['ID'],
                    '#NUMBER_BID#' => $store['NUMBER_BID'],
//                    '#FIO#' => $store['FIO'],
//                    '#PHONE#' => $store['PHONE'],
//                    '#EMAIL#' => $store['EMAIL'],
                )
            );
        }

        $APPLICATION->SetTitle($title);

        if (self::isFormSubmitted()) {
            $savedStoreId = $this->processSave($store);
            if ($savedStoreId > 0) {
                LocalRedirect($this->getRedirectUrl($savedStoreId));
            }

            $submittedStore = $this->getSubmittedStore();
            $store = array_merge($store, $submittedStore);
        }

        $this->arResult =array(
            'FORM_ID' => self::FORM_ID,
            'GRID_ID' => CBiotumManufactureBidsListComponent::GRID_ID,
            'IS_NEW' => empty($store['ID']),
            'TITLE' => $title,
            'BID' => $store,
            'BACK_URL' => $this->getRedirectUrl(),
            'ERRORS' => $this->errors,
        );

        $this->includeComponentTemplate();
    }

    private function processSave($initialStore)
    {
        $submittedStore = $this->getSubmittedStore();

        $store = array_merge($initialStore, $submittedStore);

        $this->errors = self::validate($store);

        if (!$this->errors->isEmpty()) {
            return false;
        }

        if (!empty($store['ID'])) {
            $result = BidsTable::update($store['ID'], $store);

        } else {
            $result = BidsTable::add($store);


        }

        if (!$result->isSuccess()) {
            $this->errors->add($result->getErrors());
        }

        return $result->isSuccess() ? $result->getId() : false;
    }



    private function getSubmittedStore()
    {
        $context = Context::getCurrent();
        $request = $context->getRequest();

        $submittedStore = array(
            'NUMBER_BID' => $request->get('NUMBER_BID'),
           // 'ADDRESS' => $request->get('ADDRESS'),
          //  'FIO' => $request->get('FIO'),
           // 'PHONE' => $request->get('PHONE'),
           // 'EMAIL' => $request->get('EMAIL'),
           // 'ASSIGNED_BY_ID' => $request->get('ASSIGNED_BY_ID'),
        );

        return $submittedStore;
    }

    private static function validate($store)
    {
        $errors = new ErrorCollection();

        if (empty($store['NUMBER_BID'])) {
            $errors->setError(new Error(Loc::getMessage('CRMBIDS_ERROR_EMPTY_NAME')));
        }

//        if (empty($store['ASSIGNED_BY_ID'])) {
//            $errors->setError(new Error(Loc::getMessage('CRMSTORES_ERROR_EMPTY_ASSIGNED_BY_ID')));
//        } else {
//            $dbUser = UserTable::getById($store['ASSIGNED_BY_ID']);
//            if ($dbUser->getSelectedRowsCount() <= 0) {
//                $errors->setError(new Error(Loc::getMessage('CRMSTORES_ERROR_UNKNOWN_ASSIGNED_BY_ID')));
//            }
//        }

        return $errors;
    }

    private static function isFormSubmitted()
    {
        $context = Context::getCurrent();
        $request = $context->getRequest();
        $saveAndView = $request->get('saveAndView');
        $saveAndAdd = $request->get('saveAndAdd');
        $apply = $request->get('apply');
        return !empty($saveAndView) || !empty($saveAndAdd) || !empty($apply);
    }

    private function getRedirectUrl($savedStoreId = null)
    {
        $context = Context::getCurrent();
        $request = $context->getRequest();

        if (!empty($savedStoreId) && $request->offsetExists('apply')) {
            return CComponentEngine::makePathFromTemplate(
                $this->arParams['URL_TEMPLATES']['EDIT'],
                array('STORE_ID' => $savedStoreId)
            );
        } elseif (!empty($savedStoreId) && $request->offsetExists('saveAndAdd')) {
            return CComponentEngine::makePathFromTemplate(
                $this->arParams['URL_TEMPLATES']['EDIT'],
                array('STORE_ID' => 0)
            );
        }

        $backUrl = $request->get('backurl');
        if (!empty($backUrl)) {
            return $backUrl;
        }

        if (!empty($savedStoreId) && $request->offsetExists('saveAndView')) {
            return CComponentEngine::makePathFromTemplate(
                $this->arParams['URL_TEMPLATES']['DETAIL'],
                array('BID_ID' => $savedStoreId)
            );
        } else {
            return $this->arParams['SEF_FOLDER'];
        }
    }
}