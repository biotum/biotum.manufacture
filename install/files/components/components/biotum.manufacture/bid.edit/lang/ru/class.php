<?php
defined('B_PROLOG_INCLUDED') || die;

$MESS['CRMBIDS_NO_MODULE'] = 'Модуль "Заявка на производство" не установлен.';
$MESS['CRMBIDS_STORE_NOT_FOUND'] = 'Заявка на производство не существует.';
$MESS['CRMBIDS_SHOW_TITLE_DEFAULT'] = 'Заявка на производство';
$MESS['CRMBIDS_SHOW_TITLE'] = 'Заявка на производство №#ID# &mdash; #NUMBER_BID#';
$MESS['CRMBIDS_ERROR_EMPTY_NAME'] = 'Название Заявка на производство не задано.';
$MESS['CRMBIDS_ERROR_EMPTY_ASSIGNED_BY_ID'] = 'Не указан ответственный.';
$MESS['CRMBIDS_ERROR_UNKNOWN_ASSIGNED_BY_ID'] = 'Указанный ответственный сотрудник не существует.';