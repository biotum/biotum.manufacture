<?php
defined('B_PROLOG_INCLUDED') || die;

use Biotum\Manufacture\Entity\BidsTable;
use Biotum\Manufacture\Entity\ManufacturreBids;
use Bitrix\Crm\CompanyTable;
use Bitrix\Main\Context;
use Bitrix\Main\Loader;
use Bitrix\Main\Localization\Loc;
use Bitrix\Main\UI\PageNavigation;
use Bitrix\Main\UserTable;
use Bitrix\Main\Grid;
use Bitrix\Main\UI\Filter;
use Bitrix\Main\Web\Json;
use Bitrix\Main\Web\Uri;
use Bitrix\Crm;

class CBiotumManufactureBidsListComponent extends CBitrixComponent
{
    const GRID_ID = 'CRMBIDS_LIST';
    const SORTABLE_FIELDS = array('ID', 'NUMBER_BID');
    const FILTERABLE_FIELDS = array('ID', 'NUMBER_BID');
    const SUPPORTED_ACTIONS = array('delete');
    const SUPPORTED_SERVICE_ACTIONS = array('GET_ROW_COUNT');

    private static $headers;
    private static $filterFields;
    private static $filterPresets;

    public function __construct(CBitrixComponent $component = null)
    {
        global $USER;

        parent::__construct($component);

        self::$headers = array(
            array(
                'id' => 'ID',
                'name' => Loc::getMessage('CRMBIDS_HEADER_ID'),
                'sort' => 'ID',
                'first_order' => 'desc',
                'type' => 'int',
            ),
            array(
                'id' => 'NUMBER_BID',
                'name' => Loc::getMessage('CRMBIDS_HEADER_NUMBER_BID'),
                'sort' => 'NUMBER_BID',
                'default' => true,
            ),
            array(
                'id' => 'STATUS_BID',
                'name' => Loc::getMessage('CRMBIDS_HEADER_STATUS_BID'),
                'sort' => 'STATUS_BID',
                'default' => true,
            ),
//            array(
//                'id' => 'DOCUMENT_N',
//                'name' => Loc::getMessage('CRMBIDS_HEADER_DOCUMENT_N'),
//                'sort' => 'DOCUMENT_N',
//                'default' => true,
//            ),
            array(
                'id' => 'DATE_START_MANUFACTURE',
                'name' => Loc::getMessage('CRMBIDS_HEADER_DATE_START_MANUFACTURE'),
                'sort' => 'DATE_START_MANUFACTURE',
                'default' => true,
            ),
//            array(
//                'id' => 'TIME_START_MANUFACTURE',
//                'name' => Loc::getMessage('CRMBIDS_HEADER_TIME_START_MANUFACTURE'),
//                'sort' => 'TIME_START_MANUFACTURE',
//                'default' => true,
//            ),
            array(
                'id' => 'DATE_SHIPMENTREADY',
                'name' => Loc::getMessage('CRMBIDS_HEADER_DATE_SHIPMENTREADY'),
                'sort' => 'DATE_SHIPMENTREADY',
                'default' => true,
            ),

            array(
                'id' => 'PRODUCT_ID',
                'name' => Loc::getMessage('CRMBIDS_HEADER_PRODUCT_ID'),
                'sort' => 'PRODUCT_ID',
                'default' => true,
            ),
            array(
                'id' => 'COMMENT',
                'name' => Loc::getMessage('CRMBIDS_HEADER_COMMENT'),
                'sort' => 'COMMENT',
                'default' => true,
            ),
            array(
                'id' => 'BUYER_ID',
                'name' => Loc::getMessage('CRMBIDS_HEADER_BUYER_ID'),
                'sort' => 'BUYER_ID',
                'default' => true,
            ),
            array(
                'id' => 'CONSIGNEE_ID',
                'name' => Loc::getMessage('CRMBIDS_HEADER_CONSIGNEE_ID'),
                'sort' => 'CONSIGNEE_ID',
                'default' => true,
            ),
            array(
                'id' => 'WEIGHT',
                'name' => Loc::getMessage('CRMBIDS_HEADER_WEIGHT'),
                'sort' => 'WEIGHT',
                'default' => true,
            ),
            array(
                'id' => 'DEAL_ID',
                'name' => Loc::getMessage('CRMBIDS_HEADER_DEAL_ID'),
                'sort' => 'DEAL_ID',
                'default' => true,
            ),

        );

        self::$filterFields = array(
            array(
                'id' => 'ID',
                'name' => Loc::getMessage('CRMBIDS_FILTER_FIELD_ID')
            ),
            array(
                'id' => 'NUMBER_BID',
                'name' => Loc::getMessage('CRMBIDS_FILTER_FIELD_NAME'),
                'default' => true,
            ),

//            array(
//                'id' => 'ADDRESS',
//                'name' => Loc::getMessage('CRMBIDS_FILTER_FIELD_ADDRESS'),
//                'default' => true,
//            ),
//            array(
//                'id' => 'FIO',
//                'name' => Loc::getMessage('CRMBIDS_FILTER_FIELD_FIO'),
//                'default' => true,
//            ),
//            array(
//                'id' => 'PHONE',
//                'name' => Loc::getMessage('CRMBIDS_FILTER_FIELD_PHONE'),
//                'default' => true,
//            ),
//            array(
//                'id' => 'EMAIL',
//                'name' => Loc::getMessage('CRMBIDS_FILTER_FIELD_EMAIL'),
//                'default' => true,
//            ),
//            array(
//                'id' => 'ASSIGNED_BY_ID',
//                'name' => Loc::getMessage('CRMSTORES_FILTER_FIELD_ASSIGNED_BY'),
//                'type' => 'custom_entity',
//                'params' => array(
//                    'multiple' => 'Y'
//                ),
//                'selector' => array(
//                    'TYPE' => 'user',
//                    'DATA' => array(
//                        'ID' => 'ASSIGNED_BY',
//                        'FIELD_ID' => 'ASSIGNED_BY_ID'
//                    )
//                ),
//                'default' => true,
//            ),
        );

//        self::$filterPresets = array(
//            'my_stores' => array(
//                'name' => Loc::getMessage('CRMSTORES_FILTER_PRESET_MY_STORES'),
//                'fields' => array(
//                    'ASSIGNED_BY_ID' => $USER->GetID(),
//                    'ASSIGNED_BY_ID_name' => $USER->GetFullName(),
//                )
//            )
//        );
    }

    public function executeComponent()
    {
        if (!Loader::includeModule('biotum.manufacture')) {
            ShowError(Loc::getMessage('CRMBIDS_NO_MODULE'));
            return;
        }

        $context = Context::getCurrent();
        $request = $context->getRequest();

        $grid = new Grid\Options(self::GRID_ID);

        //region Sort
        $gridSort = $grid->getSorting();
        $sort = array_filter(
            $gridSort['sort'],
            function ($field) {
                return in_array($field, self::SORTABLE_FIELDS);
            },
            ARRAY_FILTER_USE_KEY
        );
        if (empty($sort)) {
            $sort = array('NUMBER_BID' => 'asc');
        }
        //endregion

        //region Filter
        $gridFilter = new Filter\Options(self::GRID_ID, self::$filterPresets);
        $gridFilterValues = $gridFilter->getFilter(self::$filterFields);
        $gridFilterValues = array_filter(
            $gridFilterValues,
            function ($fieldName) {
                return in_array($fieldName, self::FILTERABLE_FIELDS);
            },
            ARRAY_FILTER_USE_KEY
        );
        //endregion

        $this->processGridActions($gridFilterValues);
        $this->processServiceActions($gridFilterValues);

        //region Pagination
        $gridNav = $grid->GetNavParams();
        $pager = new PageNavigation('');
        $pager->setPageSize($gridNav['nPageSize']);
        $pager->setRecordCount(BidsTable::getCount($gridFilterValues));
        if ($request->offsetExists('page')) {
            $currentPage = $request->get('page');
            $pager->setCurrentPage($currentPage > 0 ? $currentPage : $pager->getPageCount());
        } else {
            $pager->setCurrentPage(1);
        }

        $bids=ManufacturreBids::getlist(array(
            'filter' => $gridFilterValues,
            'limit' => $pager->getLimit(),
            'offset' => $pager->getOffset(),
            'order' => $sort
        ));

        $requestUri = new Uri($request->getRequestedPage());
        $requestUri->addParams(array('sessid' => bitrix_sessid()));

        $this->arResult = array(
            'GRID_ID' => self::GRID_ID,
            'BIDS' => $bids,
            'HEADERS' => self::$headers,
            'PAGINATION' => array(
                'PAGE_NUM' => $pager->getCurrentPage(),
                'ENABLE_NEXT_PAGE' => $pager->getCurrentPage() < $pager->getPageCount(),
                'URL' => $request->getRequestedPage(),
            ),
            'SORT' => $sort,
            'FILTER' => self::$filterFields,
            'FILTER_PRESETS' => self::$filterPresets,
            'ENABLE_LIVE_SEARCH' => false,
            'DISABLE_SEARCH' => true,
            'SERVICE_URL' => $requestUri->getUri(),
        );

        $this->includeComponentTemplate();
    }

    private function processGridActions($currentFilter)
    {
        if (!check_bitrix_sessid()) {
            return;
        }

        $context = Context::getCurrent();
        $request = $context->getRequest();

        $action = $request->get('action_button_' . self::GRID_ID);

        if (!in_array($action, self::SUPPORTED_ACTIONS)) {
            return;
        }

        $allRows = $request->get('action_all_rows_' . self::GRID_ID) == 'Y';
        if ($allRows) {
            $dbStores = BidsTable::getList(array(
                'filter' => $currentFilter,
                'select' => array('ID'),
            ));
            $storeIds = array();
            foreach ($dbStores as $store) {
                $storeIds[] = $store['ID'];
            }
        } else {
            $storeIds = $request->get('ID');
            if (!is_array($storeIds)) {
                $storeIds = array();
            }
        }

        if (empty($storeIds)) {
            return;
        }

        switch ($action) {
            case 'delete':
                foreach ($storeIds as $storeId) {
                    BidsTable::delete($storeId);
                }
            break;

            default:
            break;
        }
    }

    private function processServiceActions($currentFilter)
    {
        global $APPLICATION;

        if (!check_bitrix_sessid()) {
            return;
        }

        $context = Context::getCurrent();
        $request = $context->getRequest();

        $params = $request->get('PARAMS');

        if (empty($params['GRID_ID']) || $params['GRID_ID'] != self::GRID_ID) {
            return;
        }

        $action = $request->get('ACTION');

        if (!in_array($action, self::SUPPORTED_SERVICE_ACTIONS)) {
            return;
        }

        $APPLICATION->RestartBuffer();
        header('Content-Type: application/json');

        switch ($action) {
            case 'GET_ROW_COUNT':
                $count = BidsTable::getCount($currentFilter);
                echo Json::encode(array(
                    'DATA' => array(
                        'TEXT' => Loc::getMessage('CRMBIDS_GRID_ROW_COUNT', array('#COUNT#' => $count))
                    )
                ));
            break;

            default:
            break;
        }

        die;
    }
}