 <?php
defined('B_PROLOG_INCLUDED') || die;

use Bitrix\Crm\CompanyTable;
use Bitrix\Main\Config\Option;
use Bitrix\Main\Grid\Panel\Snippet;
use Bitrix\Main\Loader;
use Bitrix\Main\Localization\Loc;
use Bitrix\Main\Page\Asset;
use Bitrix\Main\Web\Json;
use Academy\CrmStores\Entity\DeliveryProductTable;

function getStoreName($id){
    $store=\Academy\CrmStores\Entity\StoreTable::getById($id);
    $result=$store->fetchAll();
   return $result[0]['ADDRESS'];
}

function getDeliveryProduct($deal)
{
    $html='';

    $delivery=DeliveryProductTable::getList(
        [
        'filter'=>['DEAL_ID'=>$deal]
        ]);
    $result=$delivery->fetchAll();
    if(!empty($result)) {
        $html .= '<div style="border: 1px solid black;">
    <table class="table information_json">
            <tbody><tr>
                <th>Пункт разгрузки</th>
                <th>Вес</th>
                <th>Дата поставки</th>
                <th>Время поставки</th>
            </tr>
            <tr class="main-grid-row main-grid-row-body">

            </tr>';

        foreach ($result as $value) {

            $html .= '<tr class="main-grid-row main-grid-row-body">
                    <td class="main-grid-cell main-grid-cell-left" style="min-width: 400px;">
                       <span class="main-grid-cell-content" data-prevent-default="true"> <a class="feed-com-add-link" href="/crm/stores/'.$value['STORE_ID'].'/">' . getStoreName($value['STORE_ID']) . '</a></span>
                    </td>
                    <td class="main-grid-cell main-grid-cell-left"  ><span class="main-grid-cell-content" data-prevent-default="true">' . $value['WEIGHT'] . '</span></td>
                    <td class="main-grid-cell main-grid-cell-left" style="min-width: 100px;" ><span class="main-grid-cell-content" data-prevent-default="true">' . $value['DATE_DELIVERY'] . '</span></td>
                    <td class="main-grid-cell main-grid-cell-left"  ><span class="main-grid-cell-content" data-prevent-default="true">' . $value['TIME_DELIVERY'] . '</span></td>
                </tr>';
        }
        $html .= '</tbody></table></div>';
    }
return $html;
}


/** @var CBitrixComponentTemplate $this */

if (!Loader::includeModule('crm')) {
    ShowError(Loc::getMessage('CRMBIDS_NO_CRM_MODULE'));
    return;
}


$asset = Asset::getInstance();
$asset->addJs('/bitrix/js/crm/interface_grid.js');

$gridManagerId = $arResult['GRID_ID'] . '_MANAGER';

$rows = array();
foreach ($arResult['BIDS'] as $store) {


    $viewUrl = CComponentEngine::makePathFromTemplate(
        $arParams['URL_TEMPLATES']['DETAIL'],
        array('BID_ID' => $store['ID'])
    );
    $editUrl = CComponentEngine::makePathFromTemplate(
        $arParams['URL_TEMPLATES']['EDIT'],
        array('BID_ID' => $store['ID'])
    );
  $status=json_decode(COption::GetOptionString('biotum.manufacture', 'APPEAL_FIELD_DESTINATION'),true);
    $deleteUrlParams = http_build_query(array(
        'action_button_' . $arResult['GRID_ID'] => 'delete',
        'ID' => array($store['ID']),
        'sessid' => bitrix_sessid()
    ));
    $deleteUrl = $arParams['SEF_FOLDER'] . '?' . $deleteUrlParams;

    $rows[] = array(
        'id' => $store['ID'],
        'actions' => array(
            array(
                'TITLE' => Loc::getMessage('CRMBIDS_ACTION_VIEW_TITLE'),
                'TEXT' => Loc::getMessage('CRMBIDS_ACTION_VIEW_TEXT'),
                'ONCLICK' => 'BX.Crm.Page.open(' . Json::encode($viewUrl) . ')',
                'DEFAULT' => true
            ),
            array(
                'TITLE' => Loc::getMessage('CRMBIDS_ACTION_EDIT_TITLE'),
                'TEXT' => Loc::getMessage('CRMBIDS_ACTION_EDIT_TEXT'),
                'ONCLICK' => 'BX.Crm.Page.open(' . Json::encode($editUrl) . ')',
            ),
//            array(
//                'TITLE' => Loc::getMessage('CRMSTORES_ACTION_DELETE_TITLE'),
//                'TEXT' => Loc::getMessage('CRMSTORES_ACTION_DELETE_TEXT'),
//                'ONCLICK' => 'BX.CrmUIGridExtension.processMenuCommand(' . Json::encode($gridManagerId) . ', BX.CrmUIGridMenuCommand.remove, { pathToRemove: ' . Json::encode($deleteUrl) . ' })',
//            )
        ),
        'data' => $store,
        'columns' => array(
            'ID' => $store['ID'],
            'NUMBER_BID' => '<a href="' . $viewUrl . '" target="_self"> №'. $store['NUMBER_BID'] .' от '.$store['DATE_BID']. '</a>',
            'STATUS_BID'=> $status[$store['STATUS_BID']],
            'PRODUCT_ID' => $store['PRODUCT_NAME'],
            'DATE_START_MANUFACTURE'=>'<div> Дата </div><strong>'.$store['DATE_START_MANUFACTURE'].'</strong><div> Время</div><strong>'.$store['TIME_START_MANUFACTURE'].'</strong>',
            'DATE_SHIPMENTREADY'=>'<div> Дата </div><strong>'.$store['DATE_SHIPMENTREADY'].'</strong><div> Время </div><strong>'.$store['TIME_SHIPMENTREADY'].'</strong>',
            'BUYER_ID'=>$store['COMPANY_NAME'],
            'CONSIGNEE_ID'=>$store['CONSIGNEE_NAME'],
            'WEIGHT'=>$store['WEIGHT'],
            'DEAL_ID'=>getDeliveryProduct($store['DEAL_ID']),
            'COMMENT'=>'fgsdgf',

        )
    );
}

$snippet = new Snippet();

$APPLICATION->IncludeComponent(
    'bitrix:crm.interface.grid',
    'titleflex',
    array(
        'GRID_ID' => $arResult['GRID_ID'],
        'HEADERS' => $arResult['HEADERS'],
        'ROWS' => $rows,
        'PAGINATION' => $arResult['PAGINATION'],
        'SORT' => $arResult['SORT'],
        'FILTER' => $arResult['FILTER'],
        'FILTER_PRESETS' => $arResult['FILTER_PRESETS'],
        'IS_EXTERNAL_FILTER' => false,
        'ENABLE_LIVE_SEARCH' => $arResult['ENABLE_LIVE_SEARCH'],
        'DISABLE_SEARCH' => $arResult['DISABLE_SEARCH'],
        'ENABLE_ROW_COUNT_LOADER' => true,
        'AJAX_ID' => '',
        'AJAX_OPTION_JUMP' => 'N',
        'AJAX_OPTION_HISTORY' => 'N',
        'AJAX_LOADER' => null,
        'ACTION_PANEL' => array(
            'GROUPS' => array(
                array(
                    'ITEMS' => array(
                        $snippet->getRemoveButton(),
                        $snippet->getForAllCheckbox(),
                    )
                )
            )
        ),
        'EXTENSION' => array(
            'ID' => $gridManagerId,
            'CONFIG' => array(
                'ownerTypeName' => 'STORE',
                'gridId' => $arResult['GRID_ID'],
                'serviceUrl' => $arResult['SERVICE_URL'],
            ),
            'MESSAGES' => array(
                'deletionDialogTitle' => Loc::getMessage('CRMBIDS_DELETE_DIALOG_TITLE'),
                'deletionDialogMessage' => Loc::getMessage('CRMBIDS_DELETE_DIALOG_MESSAGE'),
                'deletionDialogButtonTitle' => Loc::getMessage('CRMBIDS_DELETE_DIALOG_BUTTON'),
            )
        ),
    ),
    $this->getComponent(),
    array('HIDE_ICONS' => 'Y',)
);

