<?php
defined('B_PROLOG_INCLUDED') || die;

$MESS['CRMBIDS_NO_CRM_MODULE'] = 'Модуль CRM не установлен.';
$MESS['CRMBIDS_ACTION_VIEW_TITLE'] = 'Просмотреть Заявку';
$MESS['CRMBIDS_ACTION_VIEW_TEXT'] = 'Просмотреть';
$MESS['CRMBIDS_ACTION_EDIT_TITLE'] = 'Редактировать Заявку';
$MESS['CRMBIDS_ACTION_EDIT_TEXT'] = 'Редактировать';
$MESS['CRMBIDS_ACTION_DELETE_TITLE'] = 'Удалить Заявку';
$MESS['CRMBIDS_ACTION_DELETE_TEXT'] = 'Удалить';
$MESS['CRMBIDS_GRID_ACTION_DELETE_TITLE'] = 'Удалить отмеченные элементы';
$MESS['CRMBIDS_GRID_ACTION_DELETE_TEXT'] = 'Удалить';
$MESS['CRMBIDS_DELETE_DIALOG_TITLE'] = 'Удалить Заявку';
$MESS['CRMBIDS_DELETE_DIALOG_MESSAGE'] = 'Вы уверены, что хотите удалить выбранную заявку?';
$MESS['CRMBIDS_DELETE_DIALOG_BUTTON'] = 'Удалить';
$MESS['CRM_ALL'] = 'Всего';
$MESS['CRM_SHOW_ROW_COUNT'] = 'Показать количество';
