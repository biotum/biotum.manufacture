<?php
defined('B_PROLOG_INCLUDED') || die;

$MESS['CRMSTORES_NO_MODULE'] = 'Модуль "Пункты разгрузки" не установлен.';
$MESS['CRMSTORES_HEADER_ID'] = 'ID';
$MESS['CRMSTORES_HEADER_NAME'] = 'Название';
$MESS['CRMSTORES_HEADER_ASSIGNED_BY'] = 'Ответственный';
$MESS['CRMSTORES_HEADER_ADDRESS'] = 'Адрес';
$MESS['CRMSTORES_HEADER_FIO'] = 'ФИО';
$MESS['CRMSTORES_HEADER_PHONE'] = 'Телефон';
$MESS['CRMSTORES_HEADER_EMAIL'] = 'E-MAIL';
$MESS['CRMSTORES_HEADER_COMPANY'] = 'Принадлежность';
$MESS['CRMSTORES_FILTER_FIELD_ID'] = 'ID';
$MESS['CRMSTORES_FILTER_FIELD_NAME'] = 'Название';
$MESS['CRMSTORES_FILTER_FIELD_FIO'] = 'ФИО';
$MESS['CRMSTORES_FILTER_FIELD_PHONE'] = 'Телефон';
$MESS['CRMSTORES_FILTER_FIELD_EMAIL'] = 'E-MAIL';
$MESS['CRMSTORES_FILTER_FIELD_ASSIGNED_BY'] = 'Ответственный';
$MESS['CRMSTORES_FILTER_FIELD_ADDRESS'] = 'Адрес';
$MESS['CRMSTORES_FILTER_PRESET_MY_STORES'] = 'Пункты разгрузки';
$MESS['CRMSTORES_GRID_ROW_COUNT'] = 'Всего: #COUNT#';