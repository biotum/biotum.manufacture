<?php
defined('B_PROLOG_INCLUDED') || die;

$MESS['CRMSTORES_NO_MODULE'] = 'Модуль "Пункты разгрузки" не установлен.';
$MESS['CRMSTORES_STORE_NOT_FOUND'] = 'Пункт разгрузки не существует.';
$MESS['CRMSTORES_SHOW_TITLE_DEFAULT'] = 'Пункт разгрузки';
$MESS['CRMSTORES_SHOW_TITLE'] = 'Пункт разгрузки №#ID# &mdash; #NAME#';