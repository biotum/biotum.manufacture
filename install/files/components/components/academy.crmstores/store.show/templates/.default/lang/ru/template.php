<?php
defined('B_PROLOG_INCLUDED') || die;

$MESS['CRMSTORES_NO_CRM_MODULE'] = 'Модуль CRM не установлен.';
$MESS['CRMSTORES_TAB_STORE_NAME'] = 'Пункт разгрузки';
$MESS['CRMSTORES_TAB_STORE_TITLE'] = 'Свойства Пункта разгрузки';
$MESS['CRMSTORES_FIELD_SECTION_STORE'] = 'Пункт разгрузки';
$MESS['CRMSTORES_FIELD_ID'] = 'ID';
$MESS['CRMSTORES_FIELD_NAME'] = 'Название';
$MESS['CRMSTORES_FIELD_ADDRESS'] = 'Адрес';
$MESS['CRMSTORES_FIELD_FIO'] = 'ФИО';
$MESS['CRMSTORES_FIELD_PHONE'] = 'Телефон';
$MESS['CRMSTORES_FIELD_EMAIL'] = 'E-MAIL';
$MESS['CRMSTORES_FIELD_ASSIGNED_BY'] = 'Ответственный';
$MESS['CRMSTORES_TAB_DEALS_NAME'] = 'Сделки';
$MESS['CRMSTORES_TAB_COMPANY_NAME'] = 'Клиенты';
$MESS['CRMSTORES_TAB_DEALS_TITLE'] = 'Связанные сделки';
