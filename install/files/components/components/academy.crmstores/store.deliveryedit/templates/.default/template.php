<?php
defined('B_PROLOG_INCLUDED') || die;

use Bitrix\Main\Config\Option;
use Bitrix\Main\Error;
use Bitrix\Main\ErrorCollection;
use Bitrix\Main\Loader;
use Bitrix\Main\Localization\Loc;

/** @var CBitrixComponentTemplate $this */

/** @var ErrorCollection $errors */
$errors = $arResult['ERRORS'];

foreach ($errors as $error) {
    /** @var Error $error */
    ShowError($error->getMessage());
}

?>

<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
echo $arResult['DATE'];

?>

<table class="table delivery">
    <tr class="delivery">
        <td>
            <select  class="form-control" id="adrrdelivery" style="width:auto;">
            </select>
            <a href="javascript:void(0);" onclick=""><span class="crm-offer-info-link pull-right">Добавить</span></a>
        </td>
        <td>
            <span class="btn btn-success plus pull-left">+</span>

        </td>

    </tr>
    <tr>
        <td id="warning" hidden>
            <h2>Выберите товар сделки и нажмите кнопку применить ниже !</h2>
        </td>
    </tr>
</table>

    <style type="text/css">
        .information_json, .information_json * {
            -webkit-box-sizing: border-box;
            -moz-box-sizing: border-box;
            box-sizing: border-box;
        }
        .table {
            width: 100%;
            max-width: 100%;
            margin-bottom: 20px;
            background-color: transparent;
            border-spacing: 0;
            border-collapse: collapse;
        }
        .pull-right {float: left;}
        .pull-left {float: right;}
        .form-control {
            display: block;
            width: 100%;
            height: 34px;
            padding: 6px 12px;
            font-size: 14px;
            line-height: 1.42857143;
            color: #555;
            background-color: #fff;
            background-image: none;
            border: 1px solid #ccc;
            border-radius: 4px;
            -webkit-box-shadow: inset 0 1px 1px rgba(0,0,0,.075);
            box-shadow: inset 0 1px 1px rgba(0,0,0,.075);
            -webkit-transition: border-color ease-in-out .15s,-webkit-box-shadow ease-in-out .15s;
            -o-transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
            transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
        }
        .btn {
            display: inline-block;
            padding: 6px 12px;
            margin-bottom: 0;
            font-size: 14px;
            font-weight: 400;
            line-height: 1.42857143;
            text-align: center;
            white-space: nowrap;
            vertical-align: middle;
            -ms-touch-action: manipulation;
            touch-action: manipulation;
            cursor: pointer;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
            background-image: none;
            border: 1px solid transparent;
            border-radius: 4px;
        }
        .btn-danger {
            color: #fff;
            background-color: #d9534f;
            border-color: #d43f3a;
        }
        .btn-success {
            color: #fff;
            background-color: #5cb85c;
            border-color: #4cae4c;
        }
        .btn-link {
            font-weight: 400;
            color: #007bff;
            background-color: transparent;
        }
    </style>




       <script type="text/javascript" src="https://code.jquery.com/jquery-2.1.3.min.js"></script>
        <script>// формируем новые поля
            var WEIGHT=null;
            var DATE=null;
            var TIME=null;
            var STORE=null;
            var ID=null;
            var total = 0;
            var UserfirldCompanyStore="UF_CRM_COMPANY_TEST";
            //var dealID=<?//=$consignee['UF_CRM_1566456322']['VALUE']?>//;
            var dealID=74;
            var PRODUCT_NAME=null;


              $('.plus').click(function(){


                var val = $("#adrrdelivery option:selected" ).val();
                let elem = document.getElementById('name['+val+']');

                  if(!elem) {

                      BX.rest.callMethod(
                          'crm.deliveryproduct.add',
                          {
                              fields: {
                                  'DEAL_ID': 74,
                                  'COMPANY_ID': 138,
                                  'STORE_ID': val,
                              },
                          },
                          function (result) {
                              var data = result.data();
                              $('.information_json_plus').before(
                                  '<td><input type="hidden" name="" class="form-control store" style="min-width: 400px;" id="name[' + val + ']" value="' + val + '" readonly >' +
                                  '<input type="text"  class="form-control text" style="min-width: 400px;" value="' + $("#adrrdelivery option:selected").text() + '" readonly >' +
                                  '</td>' +
                                  '<td><span class="btn btn-danger minus pull-right" data-id="' + data + '">&ndash;</span></td>' +
                                  '<td><input type="text" name="WEIGHT" data-id="' + data + '" class="form-control  weight" style="width: 50px;" id="information_json_val[w' + $("#adrrdelivery option:selected").val() + ']"  value="" placeholder="Вес в тоннах"></td>' +
                                  '<td><input type="date" name="DATE" data-id="' + data + '" class="form-control  date" style="width: 200px;" id="information_json_val[d' + $("#adrrdelivery option:selected").val() + ']"  value="" placeholder="Дата доставки"></td>' +
                                  '<td><input type="time" name="TIME" data-id="' + data + '" class="form-control time" style="width: 100px;" id="information_json_val[t' + $("#adrrdelivery option:selected").val() + ']"  value="" placeholder="Время доставки"></td>'
                              );

                          }
                      );
                  }else{

                      alert("Такой пункт разгрузки уже выбран!");
                  }
            });


            BX.rest.callMethod(
                'crm.company.list',
                {
                    filter:{"=ID": dealID},
                    select: [UserfirldCompanyStore]
                },
                function (result) {
                   let arr =result.data();

                        BX.rest.callMethod(
                            'crm.store.list',
                            {
                                filter:{"=ID": arr[0][UserfirldCompanyStore]},
                                select: [ "ID","NAME","ADDRESS"]
                            },
                            function (results) {
                              let dataArray =results.data();
                                let dropdown = $('#adrrdelivery');
                                dropdown.empty();
                               // dropdown.append('<option>(Не задано)</option>');
                                dropdown.prop('selectedIndex', 0);
                                for (var data in dataArray) {
                                  //  console.log(dataArray[data].ID);
                                     dropdown.append($('<option></option>').attr('value', dataArray[data].ID).text(dataArray[data].NAME+' '+dataArray[data].ADDRESS));
                                }
                            });

                });

            $(document).on('change',function(evt){

                total = 0;
                $(this).find("input.weight").each(function() {
                    total += parseInt($(this).val());
                    quantity='';//$('#deal_<?=74?>_product_editor_product_row_0_QUANTITY').val();
                    PRODUCT_NAME='';//$('#deal_<?=74?>_product_editor_product_row_0_PRODUCT_NAME').val();
                    console.log(PRODUCT_NAME);
                });
                // if (total > quantity && evt!=="load") {
                //     alert("Вес превышен от на" + (quantity - total) + ".т !");
                // }
                // if (total < quantity && evt!=="load") {
                //     alert("Веса не хватает на" + (quantity - total) + " .т !");
                // }


                // if (quantity == null) {
                //     $("#warning").show();
                //     $(".plus").hide();
                //
                // }else {
                //     $(".plus").show();
                //     $("#warning").hide();
                // }




            });


            $(document).on('change','input.form-control', function (e) {
                     if(this.attributes['data-id']) {
                        ID=this.attributes['data-id'].nodeValue;

                      }

                STORE=store_id(this.id);

                if(this.name==='WEIGHT') {
                    WEIGHT= $(this).val();


                }
                if(this.name==='DATE') {
                    DATE= $(this).val();
                }
                if(this.name==='TIME') {
                    TIME= $(this).val();
                }

                if(STORE!==null) {
                    save(ID,"<?=74 ?>", "<?= $consignee['UF_CRM_1566456322']['VALUE'];?>",PRODUCT_NAME,STORE, DATE, TIME, WEIGHT);
                }
                // console.log(ID);
                // console.log(STORE);
                // console.log(WEIGHT);
                // console.log(DATE);
                // console.log(TIME);

            });


            function save(ID,DEAL_ID,COMPANY_ID,PRODUCT_NAME,STORE_ID,DATE_DELIVERY,TIME_DELIVERY,WEIGHT) {

                BX.rest.callMethod(
                    'crm.deliveryproduct.list',
                    {
                        filter:{"=DEAL_ID":DEAL_ID,
                            "=COMPANY_ID": COMPANY_ID,
                            "=STORE_ID": STORE_ID,},
                        select: [ "ID"]
                    },
                    function (result) {
                     //   console.log(result.data());

              if (result.data().length !== 0) {
                    BX.rest.callMethod(
                        'crm.deliveryproduct.update',
                        {
                            "ID": ID,
                            fields: {
                                'DEAL_ID': DEAL_ID,
                                'COMPANY_ID': COMPANY_ID,
                                'PRODUCT_NAME':PRODUCT_NAME,
                                'STORE_ID': STORE_ID,
                                'DATE_DELIVERY': DATE_DELIVERY,
                                'TIME_DELIVERY': TIME_DELIVERY,
                                'WEIGHT': WEIGHT
                            },
                        },
                        function (result) {
                  //      console.log(result);

                        }
                    );
              }
                    }
                );


            }

            function store_id($value){
                var str=$value;
                return str.replace(/[^0-9]/gim,'');
            }


            // on - так как элемент динамически создан и обычный обработчик с ним не работает
            $(document).on('click', '.minus', function(){
                if (confirm("Вы - уверены что хотите удалсть пункт разгрузки?"))
                    if(this.attributes['data-id']) {

                        BX.rest.callMethod(
                            'crm.deliveryproduct.delete',
                            {
                                "id":this.attributes['data-id'].nodeValue
                            },
                            function (result) {
                           //     console.log(result);
                            }
                        );
                       // console.log(this.attributes['data-id'].nodeValue);
                    }
                $( this ).closest( 'tr' ).remove(); // удаление строки с полями
            });
        </script>





