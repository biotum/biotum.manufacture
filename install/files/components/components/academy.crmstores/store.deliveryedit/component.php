<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

use Academy\CrmStores\Entity\DeliveryProductTable;
use Academy\CrmStores\Entity\StoreTable;
use Bitrix\Crm\CompanyTable;use Bitrix\Main\Application;use Bitrix\Main\Web\Uri;


$arResult['DATE'] = date('Y-m-d');

//$dealID = CCrmDeal::GetByID(self::getIDEdit());
$consignee = $GLOBALS["USER_FIELD_MANAGER"]->GetUserFields("CRM_DEAL", 74);
$dbCompany = CompanyTable::getList(array('select' => array('UF_CRM_COMPANY_TEST'),
    'filter' => array(
        'ID' => $consignee['UF_CRM_1566456322']['VALUE']
    )
));

$stores2 = $dbCompany->fetchAll();
$dbStores1 = StoreTable::getList(
    array('select' => array('ID', 'NAME','ADDRESS'),
        'filter' => array("=ID" => $stores2[0]['UF_CRM_COMPANY_TEST'])
    ));
$stores3 = $dbStores1->fetchAll();


$param = implode(",", $consignee['UF_CRM_STORE_DEAL']['VALUE']);

$dbStores = StoreTable::getList(
    array('select' => array('ID', 'NAME','ADDRESS'),
        'filter' => array("=ID" => explode(',', $param))
    ));

$stores = $dbStores->fetchAll();

$delivery=DeliveryProductTable::getList(
    array('select' => array('*'),
        'filter' => array("=COMPANY_ID" =>$consignee['UF_CRM_1566456322']['VALUE'],"DEAL_ID"=>74)
    )
);
$delivery_result=$delivery->fetchAll();

//print_r($delivery_result);
$isNoValue = $fieldValue === null;
//
//
// function getIDEdit()
//    {
//        $request =Application::getInstance()->getContext()->getRequest();
//        $uriString = $request->getRequestUri();
//        $uri = new Uri($uriString);
//        $redirect = preg_replace('~\D+~','',$uri->getUri());
//
//        return  $redirect;
//    }
//
//    function getStoreInfi($id){
//
//        $consignee = $GLOBALS["USER_FIELD_MANAGER"]->GetUserFields("CRM_DEAL", self::getIDEdit());
//
//        $dbCompany = CompanyTable::getList(array('select' => array('UF_CRM_COMPANY_TEST'),
//            'filter' => array(
//                'ID' => $consignee['UF_CRM_1566456322']['VALUE']
//            )
//        ));
//
//        $stores2 = $dbCompany->fetchAll();
//        $dbStores1 = StoreTable::getList(
//            array('select' => array('ID', 'NAME','ADDRESS'),
//                'filter' => array("=ID" => $id)
//            ));
//        $stores3 = $dbStores1->fetchAll();
//
//
//       return  $stores3[0]['NAME'].'  '.$stores3[0]['ADDRESS'];
//    }

$this->IncludeComponentTemplate();
?>


