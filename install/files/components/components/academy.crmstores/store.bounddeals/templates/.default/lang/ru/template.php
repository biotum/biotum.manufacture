<?php
defined('B_PROLOG_INCLUDED') || die;

$MESS['CRMSTORES_NO_CURRENCY_MODULE'] = 'Не установлен модуль валют.';
$MESS['CRMSTORES_DEAL_ID'] = 'ID';
$MESS['CRMSTORES_DEAL_TITLE'] = 'Название Сделки';
$MESS['CRMSTORES_DEAL_OPPORTUNITY'] = 'Сумма ';
$MESS['CRMSTORES_COMPANY_ID'] = 'ID';
$MESS['CRMSTORES_COMPANY_TITLE'] = 'Название Компании';
$MESS['CRMSTORES_CONSIGNEE_TITLE']= 'Грузополучатель';
$MESS['CRMSTORES_PRODUCT_TITLE'] ='Наименование продукта';
$MESS['CRMSTORES_WEIGHT_TITLE'] ='Вес продукта';
$MESS['CRMSTORES_DATE_DELIVERY_TITLE'] ='Дата доставки';
$MESS['CRMSTORES_TIME_DELIVERY_TITLE'] ='Время доставки';




