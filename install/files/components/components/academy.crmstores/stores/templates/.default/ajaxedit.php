<?php
defined('B_PROLOG_INCLUDED') || die;

/** @var CBitrixComponentTemplate $this */

//$APPLICATION->IncludeComponent(
//    'bitrix:crm.control_panel',
//    '',
//    array(
//        'ID' => 'STORES',
//        'ACTIVE_ITEM_ID' => 'STORES',
//    ),
//    $component
//);

$urlTemplates = array(
    'DETAIL' => $arResult['SEF_FOLDER'] . $arResult['SEF_URL_TEMPLATES']['details'],
    'AJAXEDIT' => $arResult['SEF_FOLDER'] . $arResult['SEF_URL_TEMPLATES']['ajaxedit'],
);

$APPLICATION->IncludeComponent(
    'academy.crmstores:store.ajaxedit',
    '',
    array(
        'STORE_ID' => $arResult['VARIABLES']['STORE_ID'],
        'URL_TEMPLATES' => $urlTemplates,
        'SEF_FOLDER' => $arResult['SEF_FOLDER'],
    ),
    $this->getComponent(),
    array('HIDE_ICONS' => 'Y',)
);



