<?php
defined('B_PROLOG_INCLUDED') || die;

use Academy\CrmStores\Entity\StoreTable;
use Academy\CrmStores\Entity\DeliveryProductTable;
use Bitrix\Main\Context;
use Bitrix\Main\Error;
use Bitrix\Main\ErrorCollection;
use Bitrix\Main\Loader;
use Bitrix\Main\Localization\Loc;
use Bitrix\Main\UserTable;

class CAcademyCrmStoresStoreEditComponent extends CBitrixComponent
{
    const FORM_ID = 'CRMSTORES_EDIT';

    private $errors;

    public function __construct(CBitrixComponent $component = null)
    {
        parent::__construct($component);

        $this->errors = new ErrorCollection();

        CBitrixComponent::includeComponentClass('academy.crmstores:stores.list');
    }

    public function executeComponent()
    {
        global $APPLICATION;

        $title = Loc::getMessage('CRMSTORES_SHOW_TITLE_DEFAULT');

        if (!Loader::includeModule('academy.crmstores')) {
            ShowError(Loc::getMessage('CRMSTORES_NO_MODULE'));
            return;
        }

        $store = array(
            'NAME' => '',
            'ADDRESS' => '',
            'FIO' => '',
            'PHONE' => '',
            'EMAIL' => '',
            'ASSIGNED_BY_ID' => 0
        );

        if (intval($this->arParams['STORE_ID']) > 0) {
            $dbStore = StoreTable::getById($this->arParams['STORE_ID']);
            $store = $dbStore->fetch();

            if (empty($store)) {
                ShowError(Loc::getMessage('CRMSTORES_STORE_NOT_FOUND'));
                return;
            }
        }

        if (!empty($store['ID'])) {
            $title = Loc::getMessage(
                'CRMSTORES_SHOW_TITLE',
                array(
                    '#ID#' => $store['ID'],
                    '#NAME#' => $store['NAME'],
                    '#FIO#' => $store['FIO'],
                    '#PHONE#' => $store['PHONE'],
                    '#EMAIL#' => $store['EMAIL'],
                )
            );
        }

        $APPLICATION->SetTitle($title);

        if (self::isFormSubmitted()) {
            $results='';
            $savedStoreId = $this->processSave($store);
            if ($savedStoreId > 0) {
                $context = Context::getCurrent();
                $request = $context->getRequest();
                $backUrl = $request->get('return_url');
                $consignee = $request->get('consignee');

                if($consignee){
                    $companyID =$consignee;
                }else{
                    $companyID = preg_replace('~\D+~','',$backUrl);
                }

                $consignee = $GLOBALS["USER_FIELD_MANAGER"]->GetUserFields("CRM_COMPANY",  $companyID);
                array_push($consignee['UF_CRM_COMPANY_TEST']['VALUE'], $savedStoreId);
                $GLOBALS["USER_FIELD_MANAGER"]->Update("CRM_COMPANY",  $companyID, Array("UF_CRM_COMPANY_TEST"=>$consignee['UF_CRM_COMPANY_TEST']['VALUE']));


                LocalRedirect($this->getRedirectUrl($savedStoreId));
            }

            $submittedStore = $this->getSubmittedStore();
            $store = array_merge($store, $submittedStore);
        }

        $this->arResult =array(
            'FORM_ID' => self::FORM_ID,
            'GRID_ID' => CAcademyCrmStoresStoresListComponent::GRID_ID,
            'IS_NEW' => empty($store['ID']),
            'TITLE' => $title,
            'STORE' => $store,
            'BACK_URL' => $this->getRedirectUrl(),
            'ERRORS' => $this->errors,
        );

        $this->includeComponentTemplate();
    }

    private function processSave($initialStore)
    {
        $submittedStore = $this->getSubmittedStore();

        $store = array_merge($initialStore, $submittedStore);

        //$this->errors = self::validate($store);

        if (!$this->errors->isEmpty()) {
            return false;
        }

        if (!empty($store['ID'])) {
            $result = StoreTable::update($store['ID'], $store);


        } else {
            $result = StoreTable::add($store);
            $add=[];

            if (!empty($store['consignee'])) {

                $add=array(
                    'DEAL_ID'=>$submittedStore['DEAL_ID'],
                    'STORE_ID'=>$result->getId(),
                    'COMPANY_ID'=>$submittedStore['consignee']

                );
                $delivery=DeliveryProductTable::add($add);
                $event = new \Bitrix\Main\Event("academy.crmstores", "onBeforeClientsStoreAdd", ['id'=>$add['STORE_ID'],'company'=>$add['COMPANY_ID']]);
                $event->send();
            }


        }


        if (!$result->isSuccess()) {
            $this->errors->add($result->getErrors());

        }

        return $result->isSuccess() ? $result->getId() : false;
    }

    private function getSubmittedStore()
    {
        $context = Context::getCurrent();
        $request = $context->getRequest();

        $submittedStore = array(
            'NAME' => $request->get('NAME'),
            'ADDRESS' => $request->get('ADDRESS'),
            'FIO' => $request->get('FIO'),
            'PHONE' => $request->get('PHONE'),
            'EMAIL' => $request->get('EMAIL'),
            'consignee' => $request->get('consignee'),
            'DEAL_ID'=>preg_replace('~\D+~','',$request->get('return_url')),
         //   'ASSIGNED_BY_ID' => $request->get('ASSIGNED_BY_ID'),
        );

        return $submittedStore;
    }

    private static function validate($store)
    {
        $errors = new ErrorCollection();

        if (empty($store['NAME'])) {
            $errors->setError(new Error(Loc::getMessage('CRMSTORES_ERROR_EMPTY_NAME')));
        }

        if (empty($store['ASSIGNED_BY_ID'])) {
            $errors->setError(new Error(Loc::getMessage('CRMSTORES_ERROR_EMPTY_ASSIGNED_BY_ID')));
        } else {
            $dbUser = UserTable::getById($store['ASSIGNED_BY_ID']);
            if ($dbUser->getSelectedRowsCount() <= 0) {
                $errors->setError(new Error(Loc::getMessage('CRMSTORES_ERROR_UNKNOWN_ASSIGNED_BY_ID')));
            }
        }

        return $errors;
    }

    private static function isFormSubmitted()
    {
        $context = Context::getCurrent();
        $request = $context->getRequest();
        $saveAndView = $request->get('saveAndView');
        $saveAndAdd = $request->get('saveAndAdd');
        $apply = $request->get('apply');
        return !empty($saveAndView) || !empty($saveAndAdd) || !empty($apply);
    }

    private function getRedirectUrl($savedStoreId = null)
    {
        $context = Context::getCurrent();
        $request = $context->getRequest();
        $backUrl = $request->get('return_url');
        //exit;
//        if (!empty($savedStoreId) && $request->offsetExists('apply')) {
////            return CComponentEngine::makePathFromTemplate(
////                $this->arParams['URL_TEMPLATES']['EDIT'],
////                array('STORE_ID' => $savedStoreId)
////            );
//            return '/crm/deal/edit/68/';
//        } elseif (!empty($savedStoreId) && $request->offsetExists('saveAndAdd')) {
////            return CComponentEngine::makePathFromTemplate(
////                $this->arParams['URL_TEMPLATES']['EDIT'],
////                array('STORE_ID' => 0)
////            );
//            return '/crm/deal/edit/68/';
//        }


        if (!empty($backUrl)) {
            return   $backUrl.'/';
        }

        if (!empty($savedStoreId) && $request->offsetExists('saveAndView')) {
//            return CComponentEngine::makePathFromTemplate(
//                $this->arParams['URL_TEMPLATES']['DETAIL'],
//                array('STORE_ID' => $savedStoreId)
//            );

            return  $backUrl.'/';

        } else {

            return   $backUrl.'/';
        }
    }
}