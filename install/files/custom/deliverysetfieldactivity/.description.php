<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arActivityDescription = array(
	"NAME" => GetMessage("BPSFA_DESCR_NAME"),
	"DESCRIPTION" => GetMessage("BPSFA_DESCR_DESCR"),
	"TYPE" => array("activity"),
	"CLASS" => "DeliverySetFieldActivity",
	"JSCLASS" => "BizProcActivity",
	"CATEGORY" => array(
        'ID' => 'other',
        "OWN_ID" => 'other',
        "OWN_NAME" => 'other',
	),
    'ADDITIONAL_RESULT' => array('EntityFields'),
);