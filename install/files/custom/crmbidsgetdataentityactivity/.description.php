<?
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true) die();

$arActivityDescription = array(
	'NAME' => GetMessage('CRM_ACTIVITY_GET_DATA_ENTITY_NAME'),
	'DESCRIPTION' => GetMessage('CRM_ACTIVITY_GET_DATA_ENTITY_DESC'),
	'TYPE' => 'activity',
	'CLASS' => 'CrmBidsGetDataEntityActivity',
	'JSCLASS' => 'BizProcActivity',
	'CATEGORY' => array(
		'ID' => 'other',
		"OWN_ID" => 'other',
		"OWN_NAME" => 'other',
	),
	'ADDITIONAL_RESULT' => array('EntityFields')
);