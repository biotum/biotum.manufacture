<?

/**
 * @var string $mid module id from GET
 */

use Bitrix\Main\Config\Configuration;
use Bitrix\Main\Loader;
use Bitrix\Main\Localization\Loc;

global $APPLICATION, $USER;

if (!$USER->IsAdmin()) {
    return;
}

$module_id = "biotum.manufacture";
Loader::includeModule($module_id);

IncludeModuleLangFile($_SERVER["DOCUMENT_ROOT"].BX_ROOT."/modules/main/options.php");
IncludeModuleLangFile(__FILE__);

CModule::IncludeModule("iblock");
CJSCore::Init(array("jquery"));
global $APPLICATION;



    $bVarsFromForm = false;
    $aTabs = array(
        array(
            "DIV" => "index",
            "TAB" => GetMessage("APPEAL_OPTIONS_TAB_INDEX"),
            "ICON" => "appeal_settings",
            "TITLE" => GetMessage("APPEAL_TAB_INDEX_TITLE"),
            "OPTIONS" => Array(
               'STORE_DETAIL_TEMPLATE', Array(GetMessage('CRMBIDS_OPTION_STORE_DETAIL_TEMPLATE'), array('text', '50')),
               'DEAL_DETAIL_TEMPLATE', Array(GetMessage('CRMBIDS_OPTION_DEAL_DETAIL_TEMPLATE'),  array('text', '50')),
               'DEAL_UF_NAME', Array(GetMessage('CRMBIDS_OPTION_DEAL_UF_NAME'),  array('text', '20')),
               'COMPANY_DETAIL_TEMPLATE', Array(GetMessage('CRMBIDS_OPTION_STORE_COMPANY_TEMPLATE'),array('text', '50')),

        )
        ),
        array(
            "DIV" => "form",
            "TAB" => GetMessage("APPEAL_OPTIONS_TAB_FORM"),
            "ICON" => "appeal_settings",
            "TITLE" => GetMessage("APPEAL_OPTIONS_TAB_FORM_TITLE"),
            "OPTIONS" => Array(
                "APPEAL_FIELD_DESTINATION" => Array(GetMessage("APPEAL_FIELD_DESTINATION_OPTION"), Array("list")),
              //  "APPEAL_FIELD_TYPE" => Array(GetMessage("APPEAL_FIELD_TYPE_OPTION"), Array("list")),

            )
        ),
        array(
            "DIV" => "formfield",
            "TAB" => GetMessage("BID_OPTIONS_TAB_FORM"),
            "ICON" => "bid_settings",
            "TITLE" => GetMessage("BID_OPTIONS_TAB_FORM_TITLE"),
            "OPTIONS" => Array(
                "BIDS_FIELD_DESTINATION" => Array(GetMessage("BIDS_FIELD_DESTINATION_OPTION"), Array("list")),
                //  "APPEAL_FIELD_TYPE" => Array(GetMessage("APPEAL_FIELD_TYPE_OPTION"), Array("list")),

            )
        ),
        array(
            "DIV" => "rights",
            "TAB" => GetMessage("MAIN_TAB_RIGHTS"),
            "ICON" => "appeal_settings",
            "TITLE" => GetMessage("MAIN_TAB_TITLE_RIGHTS"),
            "OPTIONS" => Array()
        )
    );
    $tabControl = new CAdminTabControl("tabControl", $aTabs);

    if($REQUEST_METHOD=="POST" && strlen($Update.$Apply.$RestoreDefaults)>0 && check_bitrix_sessid()){
        if (strlen($RestoreDefaults) > 0)
            COption::RemoveOption($module_id);
        else{
            if (!$bVarsFromForm){
                foreach($aTabs as $i => $aTab){
                    foreach($aTab["OPTIONS"] as $name => $arOption){
                        $disabled = array_key_exists("disabled", $arOption)? $arOption["disabled"]: "";
                        if($disabled)
                            continue;

                        $val = $_POST[$name];

                        if($arOption[1][0]=="checkbox" && $val!="Y")
                            $val="N";

                        if ($arOption[1][0] === 'list') {
                            $val = [];
                            foreach ($_POST[$name.'_value'] as $key => $value) {
                                $val[$value] = isset($_POST[$name. '_value'][$key])?['0'=> $_POST[$name.'_text'][$key],'1'=>$_POST[$name.'_color'][$key]] : null;
                            }

                            $val = json_encode($val);

                        }

                        COption::SetOptionString($module_id, $name, $val, $arOption[0]);
                    }
                }
            }
        }

        ob_start();
        $Update = $Update.$Apply;
        require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/admin/group_rights.php");
        ob_end_clean();
    }
    $tabControl->Begin();
    ?>
    <form method="post" action="<?echo $APPLICATION->GetCurPage()?>?mid=<?=urlencode($mid)?>&amp;lang=<?=LANGUAGE_ID?>" id="options">
        <?
        foreach($aTabs as $caTab => $aTab){
            $tabControl->BeginNextTab();
            if ($aTab["DIV"] != "rights"){
                foreach($aTab["OPTIONS"] as $name => $arOption){
                    if ($bVarsFromForm)
                        $val = $_POST[$name];
                    else
                        $val = COption::GetOptionString($module_id, $name);

                    $type = $arOption[1];
                    $disabled = array_key_exists("disabled", $arOption)? $arOption["disabled"]: "";
                    ?>
                    <tr <?if(isset($arOption[2]) && strlen($arOption[2])) echo 'style="display:none" class="show-for-'.htmlspecialcharsbx($arOption[2]).'"'?>>
                        <td width="40%" <?if($type[0]=="textarea") echo 'class="adm-detail-valign-top"'?>>
                            <label for="<?echo htmlspecialcharsbx($name)?>"><?echo $arOption[0]?>:</label>
                        <td width="50%">
                            <?if($type[0]=="checkbox"){?>
                                <input type="checkbox" name="<?echo htmlspecialcharsbx($name)?>" id="<?echo htmlspecialcharsbx($name)?>" value="Y"<?if($val=="Y")echo" checked";?><?if($disabled)echo' disabled="disabled"';?>><?if($disabled) echo '<br>'.$disabled;?>
                            <?}
                            elseif($type[0]=="text"){?>
                                <input type="text" size="<?echo $type[1]?>" maxlength="255" value="<?echo htmlspecialcharsbx($val)?>" name="<?echo htmlspecialcharsbx($name)?>">

                            <?}elseif($type[0]=="textarea"){?>
                                <textarea rows="<?echo $type[1]?>" name="<?echo htmlspecialcharsbx($name)?>" style=
                                "width:100%"><?echo htmlspecialcharsbx($val)?></textarea>
                            <?}elseif($type[0]=="select"){?>
                                <?if(count($type[1])){?>
                                    <select name="<?echo htmlspecialcharsbx($name)?>" onchange="doShowAndHide()">
                                        <?foreach($type[1] as $key => $value){?>
                                            <option value="<?echo htmlspecialcharsbx($key)?>" <?if ($val == $key) echo 'selected="selected"'?>><?echo htmlspecialcharsEx($value)?></option>
                                        <?}?>
                                    </select>
                                <?}else{?>
                                    <?echo GetMessage("ZERO_ELEMENT_ERROR");?>
                                <?}?>
                            <?}elseif($type[0]=="note"){?>
                                <?echo BeginNote(), $type[1], EndNote();?>
                            <?}elseif($type[0]=="list"){?>
                                <ul data-placeholder="<li>
                                    <label>
                                    Значение
                                        <input
                                                name='<?echo htmlspecialcharsbx($name)?>_value[]'
                                                type='text'
                                                size='<?echo $type[1]?>'
                                                maxlength='255'
                                                value=''
                                        />
                                    </label>
                                    <label>

                                       Текст
                                        <input
                                                name='<?echo htmlspecialcharsbx($name)?>_text[]'
                                                type='text'
                                                size='<?echo $type[1]?>'
                                                maxlength='255'
                                                value=''
                                        />
                                    </label>
 <label>

                                       Текст
                                        <input
                                                name='<?echo htmlspecialcharsbx($name)?>_color[]'
                                                type='text'
                                                size='<?echo $type[1]?>'
                                                maxlength='255'
                                                value=''
                                        />
                                    </label>
                                 <button type='button' onclick='$(this).parent().remove()'>
                                        Удалить
                                    </button>
                                    </li>"
                                >
                                    <?php foreach (json_decode($val) as $keys=>$value) {
                                        ?>
                                        <li>
                                            <label>

                                                Значение
                                                <input
                                                    name="<?echo htmlspecialcharsbx($name)?>_value[]"
                                                    type="text"
                                                    size="<?echo $type[1]?>"
                                                    maxlength="255"
                                                    value="<?echo $keys?>"
                                                />
                                            </label>
                                            <label>
                                                Текст
                                                <input
                                                    name="<?echo htmlspecialcharsbx($name)?>_text[]"
                                                    type="text"
                                                    size="<?echo $type[1]?>"
                                                    maxlength="255"
                                                    value= "<?echo htmlspecialcharsbx($value[0])?>"
                                                />
                                            </label>
                                            <label>
                                               Цвет фона
                                                <input
                                                    name="<?echo htmlspecialcharsbx($name)?>_color[]"
                                                    type="text"
                                                    size="5"
                                                    maxlength="255"
                                                    value= "<?echo htmlspecialcharsbx($value[1])?>"
                                                />
                                            </label>

                                            <button type="button" onclick="$(this).parent('li').remove()">
                                                Удалить
                                            </button>
                                        </li>
                                        <?php
                                    }
                                    ?>
                                </ul>
                                <button type="button" onclick="$(this).siblings('ul').append($(this).siblings('ul').data('placeholder'))">Добавить</button>
                            <?}?>
                        </td>
                        <td width="20%">
                            <?if ($arOption[3]){?>
                                <p><?echo $arOption[3];?></p>
                            <?}?>
                        </td>
                    </tr>
                <?}
            }elseif($aTab["DIV"] == "rights"){
                require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/admin/group_rights.php");
            }
        }?>

        <?$tabControl->Buttons();?>
        <input type="submit" name="Update" value="<?=GetMessage("MAIN_SAVE")?>" title="<?=GetMessage("MAIN_OPT_SAVE_TITLE")?>" class="adm-btn-save">
        <input type="submit" name="Apply" value="<?=GetMessage("MAIN_OPT_APPLY")?>" title="<?=GetMessage("MAIN_OPT_APPLY_TITLE")?>">
        <?if(strlen($_REQUEST["back_url_settings"])>0):?>
            <input type="button" name="Cancel" value="<?=GetMessage("MAIN_OPT_CANCEL")?>" title="<?=GetMessage("MAIN_OPT_CANCEL_TITLE")?>" onclick="window.location='<?echo htmlspecialcharsbx(CUtil::addslashes($_REQUEST["back_url_settings"]))?>'">
            <input type="hidden" name="back_url_settings" value="<?=htmlspecialcharsbx($_REQUEST["back_url_settings"])?>">
        <?endif?>
        <input type="submit" name="RestoreDefaults" title="<?echo GetMessage("MAIN_HINT_RESTORE_DEFAULTS")?>" OnClick="return confirm('<?echo AddSlashes(GetMessage("MAIN_HINT_RESTORE_DEFAULTS_WARNING"))?>')" value="<?echo GetMessage("MAIN_RESTORE_DEFAULTS")?>">
        <?=bitrix_sessid_post();?>
        <?$tabControl->End();?>
    </form>
    <script>
        function doShowAndHide(){
            var form = BX('options');
            var selects = BX.findChildren(form, {tag: 'select'}, true);
            for (var i = 0; i < selects.length; i++){
                var selectedValue = selects[i].value;
                var trs = BX.findChildren(form, {tag: 'tr'}, true);
                for (var j = 0; j < trs.length; j++){
                    if (/show-for-/.test(trs[j].className)){
                        if (trs[j].className.indexOf(selectedValue) >= 0)
                            trs[j].style.display = 'table-row';
                        else
                            trs[j].style.display = 'none';
                    }
                }
            }
        }
        BX.ready(doShowAndHide);

    </script>

