<?php
defined('B_PROLOG_INCLUDED') || die;

CModule::AddAutoloadClasses(
    'biotum.manufacture',
    array(
        'CAllCrmBid' => 'classes/general/crm_bid.php',
        'CCrmBid' => 'classes/mysql/crm_bid.php',

    )
);

?>