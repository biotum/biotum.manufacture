<?php

namespace Biotum\Manufacture\Util;

use Bitrix\Main\DB\Result;
use Bitrix\Main\Entity\DataManager;

class RestUtil
{
    /**
     * @param DataManager $dataManager
     * @param array $getListParams
     * @return array <count, next>
     */
    public static function prepareNavResult($dataManager, $getListParams)
    {
        $total = $dataManager::getCount($getListParams['filter'] ?: array());

        $nav = array(
            'total' => $total,
        );

        if ($getListParams['offset'] + $getListParams['limit'] < $total) {
            $nav['next'] = $getListParams['offset'] + $getListParams['limit'];
        }

        return $nav;
    }
}