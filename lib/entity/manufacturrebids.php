<?php

namespace Biotum\Manufacture\Entity;

use Academy\CrmStores\Entity\DeliveryProductTable;
use Academy\CrmStores\Entity\StoreTable;
use Biotum\Manufacture\Entity\BidsTable;
use Bitrix\Catalog\Product\PropertyCatalogFeature;
use Bitrix\Crm\CompanyAddress;
use Bitrix\Crm\CompanyTable;
use Bitrix\Crm\Entity\Company;
use Bitrix\Crm\Entity\Deal;
use Bitrix\Crm\Invoice\Property;
use Bitrix\Crm\Order\PropertyValue;
use Bitrix\Iblock\PropertyEnumerationTable;
use Bitrix\Iblock\Template\Entity\ElementProperty;
use Bitrix\Iblock\Template\Entity\ElementPropertyElement;
use Bitrix\ImBot\Bot\Properties;
use Bitrix\Main\Localization\Loc;
use Bitrix\Main\UserTable;
use Bitrix\Bizproc\FieldType;
use Biotum\Manufacture\BizProc;
use Bitrix\Sale\ProductTable;

Loc::loadMessages(__FILE__);

class ManufacturreBids
{


    public static function getlistID($params = array())
    {
        $result = [];
        $dbBids = BidsTable::getList($params);
        $bids = $dbBids->fetchAll();

        $userIds = array_column($bids, 'ASSIGNED_BY_ID');
        $userIds = array_unique($userIds);
        $userIds = array_filter(
            $userIds,
            function ($userId) {
                return intval($userId) > 0;
            }
        );

        $dbUsers = UserTable::getList(array(
            'filter' => array('=ID' => $userIds)
        ));
        $users = array();
        foreach ($dbUsers as $user) {
            $users[$user['ID']] = $user;
        }

        foreach ($bids as &$store) {
            if (intval($store['ASSIGNED_BY_ID']) > 0) {
                $store['ASSIGNED_BY'] = $users[$store['ASSIGNED_BY_ID']];
            }
        }

        $result = array_replace_recursive($bids, static::getNameProduct($bids));
        $result = array_replace_recursive($result, static::GetTimeManufacture($bids));
        $result = array_replace_recursive($result, static::getNameBuyer($bids));
        $result = array_replace_recursive($result, static::getNameConsignee($bids));
        $result = array_replace_recursive($result, static::getInfoContact($bids));
        $result = array_replace_recursive($result, static::getStoreAddress($bids));
        $result = array_replace_recursive($result, static::getTimeBid($bids));
        return $result[0];

    }


    public static function getlist($params = array())
    {

        $result = [];
        $dbBids = BidsTable::getList($params);
        $bids = $dbBids->fetchAll();

        $userIds = array_column($bids, 'ASSIGNED_BY_ID');
        $userIds = array_unique($userIds);
        $userIds = array_filter(
            $userIds,
            function ($userId) {
                return intval($userId) > 0;
            }
        );

        $dbUsers = UserTable::getList(array(
            'filter' => array('=ID' => $userIds)
        ));
        $users = array();
        foreach ($dbUsers as $user) {
            $users[$user['ID']] = $user;
        }

        foreach ($bids as &$store) {
            if (intval($store['ASSIGNED_BY_ID']) > 0) {
                $store['ASSIGNED_BY'] = $users[$store['ASSIGNED_BY_ID']];
            }
        }

        $result = array_replace_recursive($bids, static::getNameProduct($bids));
        $result = array_replace_recursive($result, static::GetTimeManufacture($bids));
        $result = array_replace_recursive($result, static::getNameBuyer($bids));
        $result = array_replace_recursive($result, static::getNameConsignee($bids));
        $result = array_replace_recursive($result, static::getInfoContact($bids));
        $result = array_replace_recursive($result, static::getStoreAddress($bids));
        $result = array_replace_recursive($result, static::getTimeBid($bids));
        return $result;

    }

    public static function Create($arFields = array())
    {
        $objectResult=DeliveryProductTable::getList(
            ['filter'=>['=DEAL_ID'=>$arFields['DEAL_ID']]]);
        if (!$entityData = $objectResult->fetchAll())
        {
            return [];
        }
        foreach ($entityData as $entityDatum) {

            $arFields['DELIVERY_ID']=$entityDatum['ID'];

            $id = BizProc\BidDocument::CreateDocument('', static::getPlanDateTimeManufacture($entityDatum,$arFields));

            BidsTable::update($id, ['NUMBER_BID' => 'R' . $id,'WEIGHT'=>$entityDatum['WEIGHT']]);
        }
    }

    public static function getPlanDateTimeManufacture($delivery=array(),$arFields=array())
    {

       $date = strtotime($delivery['DATE_DELIVERY'].''.$delivery['TIME_DELIVERY'].':00');

       $data= new \DateTime(date('d-m-Y H:i:s',$date));

       $data->modify("-".static::getStoreTravelTime($delivery['STORE_ID'])." hour");

        $startM= new \DateTime($data->format('d-m-Y H:i:s'));

        $startM->modify("-".static::GetValueTimeManufacture($arFields)."hour");
        if(static::getStoreTravelTime($delivery['STORE_ID']) and static::GetValueTimeManufacture($arFields)) {
            return array_replace_recursive($arFields, [
                'DATETIME_SHIPMENTREADY' => $data->format('d-m-Y H:i'),
                'DATETIME_START_MANUFACTURE' => $startM->format('d-m-Y H:i'),

            ]);
        }
        return $arFields;

    }

    public static function getTimeBid($params){
        $result=[];
        foreach ($params as $value) {

            $result[] = ['TIME_BID' =>ConvertDateTime($value['DATE_BID'], "HH:MI", "ru"),'CREATE_DATE_BID' =>ConvertDateTime($value['DATE_BID'], "DD-MM-YYYY", "ru")];

        }
        return $result;
    }

    public static function getStoreTravelTime($id){
          $store=StoreTable::getById($id);
          if(!$result=$store->fetch()){
              return false;
          }
        return $result['TRAVEL_TIME'];
    }

    public static function getStoreAddress($params)
    {
        $result=[];
        foreach ($params as $param=>$value) {

            $delivery = DeliveryProductTable::getList(
                [
                    'filter' => ['ID' => $value['DELIVERY_ID']]
                ]);
            $storeID = $delivery->fetch();

            $store = \Academy\CrmStores\Entity\StoreTable::getById((int)$storeID['STORE_ID']);
            $storeresult = $store->fetch();

                $result[] = ['STORE_ADDRESS' =>$storeresult['ADDRESS']];

        }

        return $result;
    }

    public static function GetTimeManufacture($params)
    {
        $result=[];
        foreach ($params as $value) {
            $props=\CIBlockElement::GetProperty(
                27,

                (int)$value['PRODUCT_ID'], "sort", "asc",Array("CODE"=>"TIME_MANUFACTURE"))->Fetch();

            $result[] = ['PRODUCT_TIME_MANUFACTURE' => $props['VALUE']];

        }
        return $result;
    }

    public static function GetValueTimeManufacture($params)
    {

            $props=\CIBlockElement::GetProperty(
                27,

                $params['PRODUCT_ID'], "sort", "asc",Array("CODE"=>"TIME_MANUFACTURE"))->Fetch();

            $result= $props['VALUE'];


        return $result;
    }



    private static function getNameBuyer($params)
    {
        foreach ($params as $value) {
            $Company = \CCrmCompany::getById($value['BUYER_ID']);

            $result[] = ['COMPANY_NAME' => $Company['TITLE']];
        }
        return $result;

    }


    private static function getInfoContact($params)
    {
        foreach ($params as $value) {
            $dbResMultiFields = \CCrmFieldMulti::GetList(
                array('ID' => 'asc'),
                array('ENTITY_ID' => 'COMPANY', 'TYPE_ID' => 'PHONE', 'ELEMENT_ID' => $value['CONSIGNEE_ID'])
            );
            $dbResMultiFields2 = \CCrmFieldMulti::GetList(
                array('ID' => 'asc'),
                array('ENTITY_ID' => 'COMPANY', 'TYPE_ID' => 'EMAIL', 'ELEMENT_ID' => $value['CONSIGNEE_ID'])
            );
            $phone = $dbResMultiFields->Fetch();
            $email = $dbResMultiFields2->Fetch();


            $result[] = ['COMPANY_PHONE' => $phone['VALUE'], 'COMPANY_EMAIL' => $email['VALUE']];

        }
        return $result;

    }

    private static function getNameConsignee($params)
    {
        foreach ($params as $value) {
            $Company = \CCrmCompany::getById($value['CONSIGNEE_ID']);

            $result[] = ['CONSIGNEE_NAME' => $Company['TITLE']];
        }
        return $result;

    }


    private static function getNameProduct($params)
    {
        foreach ($params as $value) {
            $product = \CCrmProduct::GetByID($value['PRODUCT_ID']);
            $result[] = ['PRODUCT_NAME' => $product['NAME']];

        }
        return $result;
    }

    private static function getContactCompany($params)
    {
        foreach ($params as $value) {
            $product = \CCrmCompany::GetByID($value['PRODUCT_ID']);
            $result[] = $product;

        }
        return $result;
    }

    public static function getDealProductID($dealID)
    {
        $arProductRows = \CCrmDeal::LoadProductRows($dealID);

        return $arProductRows[0]['PRODUCT_ID'];
    }


    public static function getDealQuantity($dealID)
    {
        $arProductRows = \CCrmDeal::LoadProductRows($dealID);

        return $arProductRows[0]['QUANTITY'];
    }
}