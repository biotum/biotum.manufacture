<?php

namespace Biotum\Manufacture\Entity;

use Bitrix\Main\Entity;
use Bitrix\Main\Entity\DataManager;
use Bitrix\Main\Entity\IntegerField;
use Bitrix\Main\Entity\StringField;
use Bitrix\Main\Entity\ReferenceField;
use Bitrix\Main\Localization\Loc;
use Bitrix\Main\UserTable;

Loc::loadMessages(__FILE__);

class BidsTable extends DataManager
{



    public static function getTableName()
    {
        return 'biotum_manufacture_bids';
    }

    public static function getMap()
    {
        return array(
            new IntegerField('ID', array('primary' => true, 'autocomplete' => true)),
            new StringField('XML_ID'),
            new StringField('ORDER_ID'),
            new StringField('NUMBER_BID'),
            new StringField('DATE_BID'),
         //   new IntegerField('STORAGE_TYPE_ID'),
          //  new IntegerField('STORAGE_ELEMENT_IDS'),
            new StringField('DATETIME_START_MANUFACTURE'),
            new StringField('TIME_START_MANUFACTURE'),
            new StringField('DATETIME_SHIPMENTREADY'),
            new StringField('TIME_SHIPMENTREADY'),
            new IntegerField('PRODUCT_ID'),
            new IntegerField('DEAL_ID'),
            new IntegerField('DELIVERY_ID'),
            new StringField('WEIGHT'),
            new StringField('COMMENT'),
            new IntegerField('BUYER_ID'),
            new IntegerField('CONSIGNEE_ID'),
            new IntegerField('STATUS_BID'),
            new IntegerField('ASSIGNED_BY_ID'),
             new ReferenceField(
                'ASSIGNED_BY',
                UserTable::getEntity(),
                array('=this.ASSIGNED_BY_ID' => 'ref.ID')
            )
        );
    }


}
