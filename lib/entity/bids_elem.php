<?php

namespace Biotum\Manufacture\Entity;

use Bitrix\Main\Entity;
use Bitrix\Main\Entity\DataManager;
use Bitrix\Main\Entity\IntegerField;
use Bitrix\Main\Entity\StringField;
use Bitrix\Main\Entity\ReferenceField;
use Bitrix\Main\Localization\Loc;
use Bitrix\Main\UserTable;

Loc::loadMessages(__FILE__);

class BidsElemTable extends DataManager
{



    public static function getTableName()
    {
        return 'biotum_manufacture_bids_elem';
    }

    public static function getMap()
    {
        return array(

            new IntegerField('BID_ID'),
            new IntegerField('STORAGE_TYPE_ID'),
            new IntegerField('ELEMENT_ID'),

        );
    }


}
