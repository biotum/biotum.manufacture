<?php

namespace Biotum\Manufacture\Handler;

use Bitrix\Main\Localization\Loc;

class CrmMenu
{
    public static function addBids(&$items)
    {
        $items[] = array(
            'ID' => 'BID',
            'MENU_ID' => 'menu_crm_bids',
            'NAME' => Loc::getMessage('CRMBIDS_MENU_ITEM_BIDS'),
            'TITLE' => Loc::getMessage('CRMBIDS_MENU_ITEM_BIDS'),
            'URL' => '/crm/bids/'
        );
    }
}