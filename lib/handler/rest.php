<?php

namespace Biotum\Manufacture\Handler;

use Biotum\Manufacture\Entity\BidsTable;
use Biotum\Manufacture\Rest\EventRegistrator;
use Biotum\Manufacture\Rest\RestApi;
use Bitrix\Main\Entity\Event;
use Bitrix\Main\Localization\Loc;

class Rest
{
    public static function registerInterface()
    {
        Loc::getMessage('REST_SCOPE_ACADEMY.CRMSTORES');

        $eventOnAdd = new Event(BidsTable::getEntity(), BidsTable::EVENT_ON_AFTER_ADD, array(), true);
        $eventOnUpdate = new Event(BidsTable::getEntity(), BidsTable::EVENT_ON_AFTER_UPDATE, array(), true);
        $eventOnDelete = new Event(BidsTable::getEntity(), BidsTable::EVENT_ON_AFTER_DELETE, array(), true);

        return array(
            'biotum.manufacture' => array(
                'crm.bid.list' => array(RestApi::class, 'getList'),
                'crm.bid.add' => array(RestApi::class, 'add'),
                'crm.bid.get' => array(RestApi::class, 'get'),
                'crm.bid.update' => array(RestApi::class, 'update'),
                'crm.bid.delete' => array(RestApi::class, 'delete'),
                \CRestUtil::EVENTS => array(
                    'onCrmBidAdd' => array('biotum.manufacture', $eventOnAdd->getEventType(), array(RestApi::class, 'prepareEventData')),
                    'onCrmBidUpdate' => array('biotum.manufacture', $eventOnUpdate->getEventType(), array(RestApi::class, 'prepareEventData')),
                    'onCrmBidDelete' => array('biotum.manufacture', $eventOnDelete->getEventType(), array(RestApi::class, 'prepareEventData')),
                ),

                \CRestUtil::PLACEMENTS => array(
                    'CRMBID_BID_DETAILS' => array()
                ),
            )
        );
    }
}
