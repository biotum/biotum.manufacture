<?php
defined('B_PROLOG_INCLUDED') || die;

$MESS['BIOTUM_CRMBIDS.MODULE_NAME'] = 'Заявки на производство';
$MESS['BIOTUM_CRMBIDS.MODULE_DESC'] = 'Управление заявками на производстве продукции';
$MESS['BIOTUM_CRMBIDS.PARTNER_NAME'] = 'Касьянов В.П';
$MESS['BIOTUM_CRMBIDS.PARTNER_URI'] = 'https://biotum.ru';
