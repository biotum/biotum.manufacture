<?php
defined('B_PROLOG_INCLUDED') || die;

$MESS['CRMBIDS_TAB_GENERAL_NAME'] = 'Общие настройки';
$MESS['CRMBIDS_TAB_GENERAL_TITLE'] = 'Настройки модуля «Пункт разгрузки CRM»';
$MESS['CRMBIDS_OPTION_DEAL_DETAIL_TEMPLATE'] = 'Шаблон URL карточки сделки';
$MESS['CRMBIDS_OPTION_STORE_DETAIL_TEMPLATE'] = 'Шаблон URL карточки Пункта разгрузки';
$MESS['CRMBIDS_OPTION_DEAL_UF_NAME'] = 'Название пользовательского поля сделки для связи с Пунктом разгрузки';
$MESS['CRMBIDS_OPTION_COMPANY_DETAIL_TEMPLATE'] = 'Шаблон URL компании';
$MESS['CRMBIDS_OPTION_STORE_COMPANY_TEMPLATE'] = 'Шаблон URL карточки Пункта разгрузки';
$MESS['CRMBIDS_OPTION_COMPANY_UF_NAME'] = 'Название пользовательского поля привязки с Пунктом разгрузки';
$MESS["APPEAL_OPTIONS_TAB_INDEX"] = "Общие настройки";
$MESS["APPEAL_TAB_INDEX_TITLE"] = "Общие настройки модуля";
$MESS["APPEAL_OPTIONS_TAB_FORM"] = "Настройки статусов заявки";
$MESS["BID_OPTIONS_TAB_FORM"] = "Настройки полей онлайн монитора";
$MESS["APPEAL_FIELD_DESTINATION_OPTION"] = 'Статусы заявки';
$MESS["BIDS_FIELD_DESTINATION_OPTION"] = 'Поля монитора';
$MESS["BID_OPTIONS_TAB_FORM_TITLE"] = 'Настройка полей для онлайн монитора';