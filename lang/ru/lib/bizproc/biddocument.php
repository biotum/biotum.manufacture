<?php
defined('B_PROLOG_INCLUDED') || die;

$MESS['CRMBIDS_FIELD_ID'] = 'ID';
$MESS['CRMBIDS_FIELD_NUMBER_BID'] = 'Номер заявки на производство';
$MESS['CRMBIDS_FIELD_DATE_BID'] = 'дата создания заявки';
$MESS['CRMBIDS_FIELD_STATUS_BID'] = 'Статус заявки';
$MESS['CRMBIDS_FIELD_PRODUCT_ID'] = 'ID Продукта';
$MESS['CRMBIDS_FIELD_DATE_START_MANUFACTURE'] = 'дата начала производства';
$MESS['CRMBIDS_FIELD_TIME_START_MANUFACTURE'] = 'время начала производства';
$MESS['CRMBIDS_FIELD_DATE_SHIPMENTREADY'] = 'дата готовности к отгрузке';
$MESS['CRMBIDS_FIELD_TIME_SHIPMENTREADY'] = 'время готовности к отгрузке';
$MESS['CRMBIDS_FIELD_DEAL_ID'] = 'ID Сделки';
$MESS['CRMBIDS_FIELD_DELIVERY_ID'] = 'ID доставки';

$MESS['CRMBIDS_FIELD_ASSIGNED_BY_ID'] = 'Ответственный';

$MESS['CRMBIDS_GROUP_AUTHOR'] = 'Ответственный';